Lesson #5: Safety gear, skates, and accessories
===============================================

*by Stephen Lorimor / Axis of Stevil*

`Watch this lesson on YouTube <https://www.youtube.com/watch?v=qgeNfd7J6X0&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7&index=8>`_

Last updated: January 9, 2019


Mandatory Safety gear
---------------------

- Required gear: helmet, elbow pads, knee pads, wrist guards, and mouthguard
  
   - Mouthguards are not required for referees
   - Skaters are not required to wear safety gear that provides no medical benefit
     
      - Example: a wristguard for a skater with a missing hand
      - Example: a mouthguard for a skater missing so many teeth it cannot be fitted properly
	
- Helmet, elbow pads, knee pads, wrist guards
  
   - Must have a hard shell or insert
   - Partially cracked shells can be taped over
   - Fully cracked shells must be replaced and are not allowed
   - Loose velcro on pads or wrist guards must be taped down at the referee’s discretion.
     
- Helmet
  
   - Cage-style face shields are not permitted
   - Visors are legal provided they are designed for use with the helmet
   - Visors must be transparent, meaning the wearer can be distinctly seen
   - Mirrored and iridium visors are not permitted
   - Visors must be fixed in place during game play (ie; not raised).
   - Helmet should have little play when moved side-to-side or front-to-back
   - Chin strap should not impede breathing but should be tight enough to prevent sliding
   - Straps should form a short V under the ear, not a long V under the neck.
     
- Elbow pads / knee pads
  
   - Pads should be secure on the arm/leg when lightly tugged up and down
   - Pads should be properly centered over the elbow/knee, not overly high or low.
   - Elbow pads with a padded (not hard) shell are not allowed
   - This includes pads that are soft until impact (at which point they become a hard shell).
     
- Wrist guards
  
   - When palms are up, the hard shells or insert may be on the top, bottom, or both.
     
- Mouth guard
  
   - Trimming of mouth guard is allowed, but it must be in one piece.


Optional safety gear
--------------------

- May not have a hard shell
  
   - Padded shorts
   - Knee or ankle supports
     
- May have a hard shell
  
   - Chin guards
   - Turtle shell bras
   - Shin guards
   - Tailbone protectors
   - Form-fitting face shields (ex: nose guard)


Skates, accessories, and casts
------------------------------

- Skates must be quad-style skates.  Players are not allowed to use in-line skates, although referees may use them.
- Skates are not safety gear. They are entirely the purview of the skater.
- Skates must be functional and intact. A jam may be called off or a skater may be ordered from a jam due to equipment failure at the referee’s discretion.
- Toe stops are not required equipment.
- Accessories are permitted so long as they have no sharp edges, internal wires, or anything capable of harming another skater.
- Jewelry, belts, safety pins, spikes, or anything that might pose a hazard to other skaters are disallowed.
- Casts are permitted if they do not pose a hazard to other skaters.
  
   - They do not require padding.
   - A wrist guard is not required over a cast that extends to the wrist.


Other rules and requirements
----------------------------

- Roster numbers
  
   - May not be affixed to the uniform with safety pins (hazard) or tape (falls off)
     
- Captains and alternates
  
   - The captain and alternate must sport a C (or A) on their uniform, clothing, or arm.
     
- All off-skates personnel (NSOs, photographers, coaches, etc.) must wear closed-toed shoes
  
   - Exception: those beyond 15’ from the outside track line or behind a raised barrier.
     
- A skater whose injury alters the flow of the game must sit out the next three jams.
  
   - This includes stopping the game clock, causing a jam to prematurely end, or requiring a substitution in the penalty box.
   - A second occurance from the same skater in a period requires they sit out the remainder of the period.


Penalties related to safety gear
--------------------------------

- “Illegal procedure” + optional cue indicating the violation
  
   - For removing required safety equipment during a jam (including to/from the box)
   - For removing required safety equipment while in the penalty box.
     
      - Mouthguards may be removed while seated in the box, but must be replaced prior to exiting.
      - Straps can be adjusted without penalty
      - A skater in the box may not remove gear to fix or adjust it. To do so, after being released from the box they must leave the track area.
	
- This includes both during and between jams.
