Lesson #8: Outside pack referees
================================

*by Stephen Lorimor / Axis of Stevil*

`Watch this lesson on YouTube <https://www.youtube.com/watch?v=bAZ4RRwrTg0&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7&index=10>`_

Last updated: May 22, 2018


Duties
------

- (priority) Call penalties on actions easily missed by or difficult to see from the inside

   - Cutting the track
   - Blocking with elbows, forearms, hands
   - Back blocks when the skater is angled to face the inside of the track
   - Illegal position (skating out of bounds)
   - Illegal contact (out of bounds blocking)
   - Illegal contact (out of play blocking) during no pack situations
   - Star pass violations and pass interference
   - High blocks near the inside line
   - Illegal re-entry from the penalty box
   - Skaters taking an illegal route to the penalty box
   - False start warnings and failure to yield penalties if they fail to yield

- Communicate to the refs on the inside of the track

   - Nod to indicate a skater avoided an illegal action
   - No earned pass hand signal to indicate a pass was neither legal nor a penalty
   - Tap helmet to indicate a jam is being called off

- Miscellaneous duties that might otherwise distract the inside staff

   - Check on potentially injured skaters in or near the outside of the track
   - Removing debris from the track
   - Looking for and enforcing instructions from the penalty box manager


Don't forget...
---------------

- When a skater does not hear your call, follow these steps:

   - Chase 	them down on the outside and repeat the call. This overrides your normal coverage.

      -	This may involve skating clockwise around the track to meet a counterclockwise moving jammer.

   - Communicate the penalty to the inside referees.
   - Communicate the penalty to another OPR in better position to make the call
   - (Last resort) Skate to the inside and call the penalty from there

- Focus heavily on jammer interaction as the jammer comes through your coverage area
- Listen for calls from other referees

   - Two referees calling out the same penalty is redundant; one can remained focused on the game

- OPRs rarely define the pack or issue out of play warnings, but sometimes it is necessary


Tips
----

- Accelerate before the pack reaches your position.
- Accelerate into turns to avoid falling behind the pack
- Always be aware of the location of the other OPRs
- “Leapfrog” (ie; pass and assume their position) other officials as necessary.
- Watch for skaters (team staff, audience members, etc.) in the outside safety lane


Three referee coverage
----------------------

- Fluid half-lap - The rear OPR falls back from turns 2 and 4 to turns 1 and 3.

   - This works in reverse if the pack is sprinting clockwise (rare)

- Sectional - Three refs on the pack at all times. Used for slow packs. Shift to fluid if it speeds up.


Two referee coverage
--------------------

- Both refs on the pack, but as a ref falls behind they fall back until the pack catches up


One referee coverage
--------------------

- Sprint like hell and pray for slow derby


Homework
--------

- Watch Ref-Ed’s great videos on OPR coverage
- Practice the fluid half-lap rotation


Skating skills
--------------

- Fast skating
- Backwards skating