# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'The Zen of Reffing'
copyright = '2023, Axis of Stevil, Luna, Blocktopus'
author = 'Axis of Stevil, Luna, Blocktopus'
release = '1.0'


# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ['rst2pdf.pdfbuilder']

templates_path = ['_templates']
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'alabaster'
html_static_path = ['_static']


# -- Options for PDF output --------------------------------------------------

# (source start file, target name, title, author, documentclass).
pdf_documents = [('index',
                  u'rst2pdf',
                  u'The Zen of Reffing Roller Derby',
                  u'Axis of Stevil',
                  'manual'
),]
pdf_stylesheets = ['sphinx', 'a4']
pdf_language = "en_US"
pdf_break_level = 1
pdf_use_index = False
pdf_toc_depth = 2
