Lesson #10: Jammer referees
===========================

*by Stephen Lorimor / Axis of Stevil*

`Watch this lesson on YouTube <https://www.youtube.com/watch?v=vp0t7phwhNw&t=0s&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7&index=12>`_

Last updated: January 9, 2019


Duties
------

- Call off the jam in response to the lead jammer’s call off signal
- Declare lead jammer status the instant it is earned.
- Track earned passes on each trip through the pack
- Report points earned after each scoring trip through the pack
- Issue penalties on the jammer and skaters engaging the jammer
- Monitor your jammer on their way to, from, and inside the penalty box

   - Watch for safety gear and penalty box violations
   - Do not miss their departure from the box
   - Verify they legally re-enter the track

- Watch for nodding or “no earned pass” signals from OPRs when your jammer skates near the outside track boundary
- Watch your jammer for false starts
- Verify your jammer’s team is fielding a jammer in visible possession of a Star cover.

   - Issue appropriate warnings if your jammer’s team is not in compliance
   - If your jammer’s team fails to field a jammer, issue a delay of game penalty to their captain

- Communicate unusual situations to the IPRs (e.g., “too many skaters”)
- Wear a colored wristband on your left arm that matches the jammer’s team

   - JRs and scorekeepers switch teams and wristbands at halftime.


Lead and Not Lead
-----------------

- When a jammer earns lead jammer status, their JR should blow two short whistle blasts in rapid succession. Signal lead jammer until they enter the engagement zone of the first scoring trip. Continue pointing at the jammer for the remainder of the jam or until lead jammer status is lost.
- When a jammer fails to earn lead jammer status, the jam referee usually spits out their mouth whistle. Signal “not lead jammer” at the start of the jammer’s first scoring trip until they enter the rear of the engagement zone.
- What happens if the jammer falls back into a prior trip through the pack
- What happens if a jammer gets lead (but does not exit the EZ) then goes to the box?


Calling off the Jam
-------------------

- Do not call off the jam until the second tap of the hips, not the first
- You’re committed If you blow the first four call-off whistles -- finish the sequence.
- JRs does not use the jam call-off hand signal unless their jammer called off the jam.
- Learn to recognize moments when jammers tend to call the jam

   - Upon completing a trip through the pack
   - Upon the other jammer entering the rear of the engagement zone
   - Upon falling or being knocked out of bounds
   - When the other jammer is standing in the penalty box
   - When both jammers are racing around the track in close proximity


Enforcing penalties
-------------------

- Focus penalty calling on your jammer and the skaters engaging them
- When sending a jammer off the track, repeat the hand signal for the skater’s team staff and/or the game announcers. Not required, but this is a professional courtesy.


Scoring
-------

- Scoring continues until the fourth whistle of the first set of call-off tweets
- Communicate points scored to the scorekeeper (bench, fans, etc.) using hand signals.
- Points should be held up from engagement zone to engagement zone and for a minimum of 5-7 seconds at the end of each jam. This includes when no points are scored.
- Track which opponents have (or have not) been scored upon
- Observing the penalty box and the to/from the pack pathways between scoring trips makes tracking NOTT points much easier.
- Develop a system that works for you to remember which blockers your jammer has legally passed (and/or not passed). Make sure it works even after a minute or more in the penalty box.


Working with your scorekeeper
-----------------------------

- Meet with your scorekeeper before each game

   - Demonstrate how you signal points (including your “did not complete initial trip” signal).

- Teach this to your scorekeeper: if you tap your wrist while your jammer is in the penalty box, it means you forgot if your jammer is on their initial or a scoring trip. The scorekeeper should respond with either your “did not complete initial trip” hand signal or the scoring trip number.
- After each scoring trip verify the scorekeeper recorded the correct number of points
- (Between jams), if the scoreboard displays an incorrect score, work with your scorekeeper to identify and fix the error.
- Check your scorekeeper’s math at the end of each period.


Whistles
--------

- JRs have special whistle needs. Approaches vary, but an excellent one is:

   - Both JRs start with a whistle on a lanyard in their mouths
   - Not-lead JR spits out the whistle after lead jammer is declared
   - Lead JR spits out the whistle if lead jammer status is lost
   - Both referees also wear and use a finger whistle

- If you choose to only use a whistle on a lanyard, have a backup whistle on your person
- When blowing your whistle may be imminent (ex: calling off the jam), have an inhaled breath ready. Even a half-second delay may impact the game.


Tips
----

- Listen to the IPRs for important announcements
- Never look away when your jammer is in the pack.
- Learn your league or area’s standard practice for jam reffing. My personal preference:

   - During the initial trip through the pack, tuck your left arm behind your back to communicate that no lead jammer status has been established.
   - Maintain this pose if your jammer is sent to the penalty box during the initial trip.
   - Keep score at your side during scoring trips.

- JRs should usually be parallel with their jammers (not ahead or behind)


Skating skills
--------------

- Fast starting (ex: toe-stop start)
- Fast stopping (ex: hockey stop)
- Abrupt direction changes
- Shuffles 
- Moving laterally 