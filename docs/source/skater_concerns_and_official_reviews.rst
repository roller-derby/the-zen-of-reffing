Lesson #13: Skater concerns and official reviews
================================================

*by Stephen Lorimor / Axis of Stevil*

`Watch this lesson on YouTube <https://www.youtube.com/watch?v=UghG3Xe5Hwk&t=0s&index=15&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7>`_

Last updated: January 9, 2019


Skater (coach, etc.) concerns
-----------------------------

- If a skater has a fast question or concern between jams, help as time allows.

   - Answering skater questions is a low priority task. If you’re busy, say so.
   - Any question that requires more than a five word answer is probably too long.
   - Call an official timeout as concerns warrant (ie: track damage presenting a safety hazard)

- Don’t lose control of the conversation. As soon as a skater asks you to rehash the last jam, suggest their team seek an official review to discuss the matter.
- If the team uses a Team Timeout, they can “buy” the head referee’s time for a minute.

   - This is only to hear out their concern(s); it is not a free official review.


Official reviews
----------------

- Each team receives one official review per period.

   - This is their time to spend as they wish. It warrants your full attention and respect.
   - If you cannot be of assistance during a review, you may be asked to return to your duties.
   - The subject of the review must be about the prior jam, the lineup for the prior jam, or a scoring issue from two jams prior updated on the scoreboard during the prior jam.

- Official reviews must be requested by the Captain or Alternate

   - They must be wearing a visible “C” or “A”.
   - They may not be inside the penalty box or on their way to it.
   - Successfully violating this procedure warrants a delay of game penalty to the captain.

- Outside pack referees should skate to the inside whenever a team requests an official review.
- Officiating issues from the prior jam should be resolved prior to the beginning of the review.
- A team has the option of exchanging their review for a 60-second “timeout”.

   - This may not be changed to a “normal” review once significant time has elapsed.

- If a team’s first official review of a period results in a finding of officiating error, the team’s official review is “refunded” and may be used again. This may happen to each team once per half.
- There is no standard practice for official reviews.

   - Every head referee has their own style. Some are quite different from others.
   - If you are new to working with a head referee, ask how they conduct official reviews

- The head referee has control during an official review. Even if you believe you can solve an issue, do not interrupt the head referee or team captains.

   - Asking a clarifying question of the team captains is usually acceptable.

- The head referee will try to establish a consensus regarding the review.

   - The head referee makes the final decision.
   - The head referee notifies the team captains as to the verdict.
   - It is good form to notify the announcers as to the nature and result of the review.

- You will make bad calls that will be overturned.

   - Learn from your mistakes.

- You will make good calls that will be overturned.

   - Sometimes the overturning is correct, sometimes it isn’t.
   - Remember that your perspective is but one view of an event. It is not the entire truth.
   - It is not appropriate for referees to publicly air grievances regarding official reviews.

- Don’t let an overturned call affect your focus or professionalism.
