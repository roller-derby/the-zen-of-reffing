Lesson #16: Advanced scoring
============================

*by Stephen Lorimor / Axis of Stevil*

`Watch this lesson on YouTube <https://www.youtube.com/watch?v=uh2w9FJZ72I>`_ **(updated!)**

Last updated: January 9, 2019


All referees should be able to recite and explain these six ways on demand.
(There used to be eight ways -- consider yourself lucky!)


Six Ways to Score a Point
-------------------------

1. Passing an opposing blocker at the hips.
2. Opponents who skate clockwise past a downed jammer.
3. NOTT (“not on the track”) points (2.5)
   
   a. There are five types of possible NOTT points.
      
      i. Opponents on their way to or from the penalty box, or serving time within it
	 
         1. This does not include penalized opponents instructed to remain on the track
	    
      ii. Opponents off the track due to injury
      iii. Opponents not participating in the jam (ie; quit the jam or were never a part of it)
      iv. Opponents who left the track temporarily due to equipment or skate issues
      v. Opponents who returned to the track (from the box or equipment failure.) behind the jammer.
	 
   b. (common) Earned upon earning a pass upon the first opposing Blocker during each scoring trip.
      
      i. This doesn’t have to passing at the hips.  It can be any of the other ways to score a point.
	 
   c. (rare) If a scoring trip presents no opportunity for a jammer to score because all opposing blockers illegally defend their points, all NOTT points are earned at the end of a scoring trip.
   d. Once NOTT points are awarded, the jammer automatically receives a point for any unscored-upon blocker who becomes a NOTT point.
   e. NOTT points not earned because the jammer was in the box or not wearing the star are earned once the jammer is able to score again.
      
4. Out of Play Blockers to the front of the engagement zone when the jammer completes a trip through the pack by exiting the front of the engagement zone.
5. Out of Play Blockers to the front of the engagement zone when the jam ends.
6. A jammer who returns to the track from the penalty box ahead of an out of play (rear) blocker.
