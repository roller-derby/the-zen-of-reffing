Lesson #11: The head referee
============================

*by Stephen Lorimor / Axis of Stevil*

`Watch this lesson on YouTube <https://www.youtube.com/watch?v=XeUOS9fGOvU&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7&index=13>`_

Last updated: January 21, 2018


Duties (pre- and post-game)
---------------------------

- Conducts pre-game and halftime officials’ meetings
- Meets with the captains prior to the game and at halftime.
- Re-assigns referee positions as necessary (ex; official becomes injured, doesn’t show up, etc.)
- In conjunction with the head NSO, verifies that NSO positions are adequately staffed.
- Confirms the EMTs are present. Discusses an emergency action plan with them.
- Confirms the track is correctly set up.
- Provides verbal feedback to referees and NSOs
- Fills out certification evaluations for crew members when requested or appropriate
- Signs IGRF (sanctioned games only)


Duties (during a game)
----------------------

- Monitors the penalty queue. Sends skaters in the queue to the box as space allows.
- Manages official reviews

   - Seeks consensus among the referee staff, but serves as final arbiter of the game

- Ensures accuracy of the score and game clocks
- Enforces and oversees skater expulsions

   - The head referee is the only person with authority to expel a skater, coach, or team staff member from the game.

- Re-assigns officials if they are unable to perform or poorly performing their duties.
- Remain calm and professional at all times.

   - True for all officials, double-true for head referees.


Duties (league)
---------------

- Supervises the league’s referee staff

   - Recruitment, training, assessment, retention, conflict resolution, etc.

- Assists the coaching staff and team captain with rules training for skaters
- Assists the head NSO with rules training for the NSO staff
- Coordinates scheduling of the referee staff for games and scrimmages

   - Communicates in advance the schedule, assignments, expectations, dress code, etc. to participating referees

- Serves as an officiating liaison to the league administration


Don't forget...
---------------

- Not all game head referees are league head referees (and vice-versa).
- A game head referee traditionally serves as rear IPR (common) or front IPR (less common).
- The head referee should have an exceptional mastery of the game’s rules. The rules do not allow the head referee to invent, ignore, or alter existing rules.
