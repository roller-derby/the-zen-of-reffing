Lesson #9: Inside pack referees
===============================

*by Stephen Lorimor / Axis of Stevil*

`Watch this lesson on YouTube <https://www.youtube.com/watch?v=SIE5rV0X7xA&index=11&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7>`_

Last updated: January 21, 2018


Front and rear IPR duties
-------------------------

- Focus on failure to reform, failure to return, and destruction penalties related to their side of the engagement zone
- Focus on penalties committed by pack blockers, particularly against jammers
- Call penalties on jammers as necessary

   - Good form dictates giving a moment for the jammer ref to call the penalty first.
   - Be 100% sure when making a call on a jammer as an incorrect call can have a major impact on the game.

- Communicate pack status, in/out of play, and no earned pass information to the jammer referees
- An IPR (usually the rear) almost always serves as the game’s head referee


Rear IPR (IPR-R) duties
-----------------------

- Holds primary responsibility for defining the pack
- IPR-R has an ideal view to issue penalties for:

   - Back blocks
   - Failure to yield after a jammer line false start

- Watch blockers for legal pack re-entry

   - Returning from the penalty box
   - Returning to play having temporarily removed themselves from the game due to equipment failure, injury, etc.

- As requested by the box manager, send blockers in the queue back to the box
- Between jams, confirm skaters in the penalty box queue enter the next jam in the correct position


Front IPR (IPR-F) duties
------------------------

- Defines the pack when the IPR-R needs assistance

   - Often occurs when there is an out of play situation to the rear
   - When the IPR-R is confused or has erred

- Watches closely for these penalties

   - Multiplayer blocks
   - Skaters using elbows/forearms to “swim” through the pack

- Communicate lead is open / closed to the jammer referees
- If appropriate, offer a fast assessment who should have lead jammer status (“yellow was first”)
- If necessary, chases down the Pivot following incomplete or illegal star passes

   - Important when the Pivot is under the incorrect belief they are the Jammer.


Tips
----

- The IPR-F should remain ahead of or even with the foremost blocker.
- The IPR-F primarily skates backward. Good backward skating skills are important.
- Skating a few feet away from the track line not only gives room for the jammer refs to pass, it widens your field of vision allowing you to see more action on the track.
- If you notice a jammer referee is inconsistent about dropping the whistle from their mouth when covering a not-lead jammer, add their name to your lead is closed call to subtly (and politely) call their attention to your declaration. (ex; “Lead is closed, Stevil”)


Skating skills
--------------

- Backwards skating for the Front IPR
- Fast transitions between forwards and backwards, clockwise and anti-clockwise skating, to follow the shape of the pack
- Quick stepping skills to stay out of the way of the Jammer Ref lane
