Lesson #7: Referees (all positions)
===================================

*by Stephen Lorimor / Axis of Stevil*

`Watch this lesson on YouTube <https://www.youtube.com/watch?v=1gGrbVFWvCU&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7&index=9>`_

Last updated: May 22, 2018


Game-related duties
-------------------

- Assist with setting up the track
- Attend pre-game and halftime officials' meetings
- Call out penalties as observed

   - If a referee is in doubt on a call, a penalty must not be called. (5.4)
   - If intent must be inferred but is not clear, legal intent must be presumed.

- Echo jam call-off whistles / hand signals (for an exception see lesson #10)
- Call official time-outs as necessary to protect safety and proper game play
- Participate in official reviews
- Call off jams early if necessary for safety

   - Maybe call off for: minor injury, skater equipment failure, penalty that gives one side an unfair advantage, nearby emergency, spectator interference, or spills/debris on the track
   - Always call off for: injury that presents an immediate danger, technical or mechanical difficulty, venue malfunction (ie; power outage), fighting, or physical interference by anyone not in the jam (including fans on the track).

- Between jams / at the jam start

   - Communicate penalties from the prior jam with the penalty tracker as necessary
   - Watch for false starts and early/late hitting
   - Watch for too many skaters (pivots, jammers, etc.)
   - Bring major issues to the attention of the head referee
   - Provide warnings for technical infractions (too many skaters, potential false starts, missing skater numbers, etc.). Do not provide warnings after the five second pre-jam announcement.
   - (low priority) Address non-officials’ minor issues/questions/requests as time and your duties allow.


Tips and reminders
------------------

- Skaters (team staff, spectators, etc.) will aggravate you, sometimes on purpose. Any response should be professional and done within the rules.
- Referees are not coaches. Our duty is to ensure fair play, not to assist a team or individual skater.
- Referees must wear safety gear at all times while standing on skates.

   - Exception: removing one’s helmet during a national anthem.
   - Exception: safety gear that provides no medical benefit

- Only the lead jammer ref should initially call off the jam in response to a jammer’s call-off signal.
- There are ideally 2 inside pack, 2 jammer, and 3 outside pack referees.


Scope of officiating
--------------------

- The scope of what we can and cannot officiate is not well defined.

   - When do we begin and end officiating during a game?
   - When do we prevent a skater from participating due to injury?
   - At what point do we bar a skater from participating due to equipment failure?

- At some point our authority ends and skaters are responsible for themselves. The line of demarcation is not clear.
