Lesson #18: Three types of penalties
====================================

*by Stephen Lorimor / Axis of Stevil*

`Watch this lesson on YouTube <https://www.youtube.com/watch?v=DoJ6Iw0d7gA&index=20&t=0s&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7>`_

Last updated: June 4, 2018


Contact penalties
-----------------

- Impact to an illegal target zone

   - Initiating a block to an opponent’s back, including the buttocks and back of the legs
   - Initiating a block to an opponent above the shoulders
   - Initiating a block to an opponent in the side or front of their legs (below mid-thigh)

- Impact with an illegal blocking zone

   - Initiating a block with one’s hands, forearms, or elbows

      - Exception: these are legal when pulled into the torso

   - Initiating a block with one’s legs (mid-thigh or below)
   - Initiating a physical or positional block with the head

- Other illegal contact

   - Blocking between jams
   - Blocking or assisting to or from a downed skater
   - Blocking or assisting to or from out of bounds
   - Blocking or assisting to or from out of play
   - Blocking or assisting while stopped or skating clockwise

- Multiplayer blocks

   - Two skaters grasping, linking, or using an impenetrable wall, preventing an opponent from passing between them


Game structure penalties
------------------------

- Illegal positioning

   - Illegally exiting the track or gaining speed outside of the track area
   - Assuming a downed position to avoid a block
   - Starting a jam completely outside of the legal starting area
   - Failing to yield advantage after starting the jam partially outside the legal starting area
   - Destruction (ie; destroying the pack)
   - Failing to return to the engagement zone
   - Failing to reform during a no pack scenario

- Gaining position

   - Illegally gaining position after re-entering from out of bounds
   - Illegally gaining position after returning to gameplay from the penalty box or fixing equipment

- Interfering with the flow of the game

   - Illegally preventing a jam from beginning
   - Calling off a jam while not lead jammer
   - A fouled out skater interfering with a jam
   - Forcing a jam to be called off due to too many skaters

- Other illegal procedures

   - Various infractions involving the penalty box
   - Various infractions involving star passes and control of helmet covers
   - Improper skates, uniforms, or jewelry
   - Improper use of safety gear
   - Miscellaneous technical infractions
   - Preventing a star pass via an illegal action


Unsporting conduct penalties
----------------------------

- Misconduct and insubordination

   - Misconduct

      - Impersonating an official
      - Physical or positional contact outside the range of what is acceptable for the sport

- Blocking while airborne
- Reckless entry into the penalty box
- Intentional jabbing with the elbow or knee
- Certain positional head blocks
- Positionally blocking a downed skater preventing them from standing
- Removing an opponent’s helmet cover from their head

      - Hiding the star
      - Faking impact

- Intentional falling after being contacted to draw a penalty

      - Adopting a downed position before a block to draw a penalty upon an opponent
      - Violating policies of the governing body association under whose jurisdiction the game is being played.

- Example: too many team staff

      - Profane, abusive, or obscene language or gestures directed at a non-official

   - Insubordination

      - Profane, abusive, or obscene language or gestures directed at an official
      - Willful or neglectful failure to obey the instructions of an official

- This is most commonly failing to leave the track after a penalty


Hierarchy of calls
------------------

- Sometimes an action warrants a penalty for multiple reasons

   - Example: leg-to-leg contact that forces an opponent down

      - Could be a low block or could be a leg block

- When an action warrants a penalty for violating multiple rules, the official should issue the penalty for which they feel most confident.
- If the official feels equally confident in multiple possible calls, use this hierarchy:

   - Misconduct (4.3)
   - Illegal Target Zone (4.1.1)
   - Illegal Blocking Zone (4.1.2)
   - Illegal Contact (4.1.3)
   - Multiplayer Block (4.1.4)
   - Illegal Position (4.2.1)
   - Gaining Position (4.2.2)
   - Interference (4.2.3)
   - Illegal Procedure (4.2.4)
