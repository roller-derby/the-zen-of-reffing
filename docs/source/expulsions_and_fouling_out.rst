Lesson #24: Expulsions and fouling out
======================================

*by Stephen Lorimor / Axis of Stevil*

`Watch this lesson on YouTube <https://www.youtube.com/watch?v=_KorPhQtKPI&index=29&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7>`_

Last updated: January 21, 2018


Fouling out
-----------

- A skater fouls out after seven penalties
- Play continues without the fouled out skater for the remainder of the jam
- The fouled out skater must leave the track area and may not remain in the team’s bench area
- A fouled out skater is not required to remove their skates or gear
- Any official can process a foul out; this is typically done by the box manager or head referee


Expulsions
----------

- Used for infractions so severe they warrant the removal of a skater from the game for a first offense
- Expulsion-level blocks need not make physical contact (ie; one can “miss”)
- Expulsion-level blocks need not have impact
- Legal blocks never warrant an expulsion
- The head referee is the only person with the authority issue an expulsion

   - Other referees should issue a penalty if appropriate and recommend the head referee upgrade the penalty to an expulsion

- Both skaters and non-skaters (coaches, team support staff) may be expelled
- Captains serve a penalty for expelled team staff, but it does not count toward fouling out
- Mitigating factors can be taken into account when deciding whether to expel someone
- Expulsions can only be issued when there is no doubt the action warrants an expulsion
- When discussing a potential expulsion, it can be useful to consult the NSO crew.
- In sanctioned games and some tournaments, particularly egregious expulsion-level actions can also result in a recommendation of suspension from future games.

   - The actual suspension determination process varies depending on the organization and tournament.


Ejections (all types)
---------------------

- Ejected skaters are typically held next to the penalty box until the jam ends
- If the jam ends before the ejected skater’s time in the penalty box expires, another skater is substituted for that skater between jams. That skater serves any remaining time in the same position (i.e.; as pivot for a pivot who fouled out).
- If a captain or designated alternate is ejected, the team may designate a new one.

   - If a penalty must be served by a (non-existent) captain, the team must designate a new one.

- An ejected skater may sit in the audience

   - The skater must behave in a manner appropriate for a member of the audience
   - If the fouled out skater interferes with game play, the captain is issued a unsporting conduct penalty and the skater is expelled back to the locker room or staging area

- Audience members that needs to be ejected should be handled in accordance with the host league’s security plan.
- In tournament environments, the skater is often escorted away from the play area (usually by the alternate referee).


Expellable offenses
-------------------

- This is not an exhaustive list.
- To an opponent

   - Negligent or reckless blocking to an illegal target zone
   - Negligent or reckless blocking with an illegal blocking zone
   - Negligent or reckless illegal contact (OOB, OOP, etc.)
   - Intentional, negligent, or reckless contact above the shoulder
   - Intentional tripping
   - Intentionally falling on a prone or downed opponent
   - Slide tackling
   - Holding or pinning a skater to the ground
   - Shoving
   - Intentionally removing an opponent’s helmet cover

- To an official

   - Forceful contact that is intentional or reasonably avoidable
   - Willfully failing to leave the floor after fouling out
   - Deliberate and excessive insubordination to an official

- To anyone

   - Contact with the head of a skater not wearing a helmet
   - Pulling of the head, neck, or helmet
   - Fighting

      - Protecting oneself without engaging the other person is legal

   - Completely losing one’s temper and/or yelling a string of abuse or profanity
   - Threatening physical violence
   - Serious physical violence or other action that causes an extraordinary physical threat to oneself or others.
   - Sexual harassment

- Other

   - Interference in gameplay by skaters or support staff not in a jam
   - Forceful contact to a non-teammate while entering the penalty box
   - Forcible contact with the penalty box seat to a non-teammate while entering the penalty box

      - This cannot be due to the structural failure of the seat
