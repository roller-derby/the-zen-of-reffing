Lesson #25: No pack situations
==============================

*by Stephen Lorimor / Axis of Stevil*

`Watch this lesson on YouTube <https://www.youtube.com/watch?v=HgiL_-nco8k&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7&index=30>`_

Last updated: June 20, 2018


Failure to reform
-----------------

- An illegal action whereby a blocker does not attempt to reform a pack
- All actions to reform the pack must be done immediately

   - Leisurely reforming is insufficient

- No penalties are issued if the pack immediately reforms
- Blockers who are directed to the penalty box cannot reform the pack
- Blockers in the back group must cease blocking and skate forward
- Blockers in the front group must cease blocking and brake to a halt

   - Blockers in the front group are not required to skate clockwise, but may do so.

- Blockers in a middle group (if any) may either stop or skate forward

   - The entire group need not perform the same action

- Out of bounds blockers who can legally re-enter must do so if reentering would help to reform the pack.
- Skaters in a position that prevents an opposing blocker from directly returning in bounds (i.e.; the opponent will receive a cut if they reenter) must skate forward until the skater may re-enter.
- Penalties go to the most responsible person (if any) in either group that fails to reform

   - Priority goes to a blocker who initiates a new block or continues an existing one

      - This includes positional blocks

   - Next priority goes to the blocker closest to the other group who fails to reform

      - This could also be a blocker who fails to move forward allowing an out of bounds skater to return to the track without skating clockwise

   - When skaters are equally responsible the penalty goes to the closest blocker

- A penalty should be issued to the most responsible blocker per team who fails to reform
- Sustained failure to reform warrants additional penalties


No pack situation handling
--------------------------

- “No pack” is announced.

   - The Rear IPR typically does this. Front IPR less often. On rare occasion an OPR.

- Destruction penalties are issued (if applicable)

   - These are typically issued by the referee who announced “no pack”
   - If a pack will reform quickly the destruction penalty is sometimes delayed until the pack reforms

- The front IPR issues failure to reform penalties to the front group as warranted
- The rear IPR issues failure to reform penalties to the rear group as warranted
- OPRs watch for blocks that cause an opponent to fall or go out of bounds
- If an immediate failure to reform penalty was issued and the pack is not yet reformed, an IPR re-calls out “No pack!”

   - Some referees consider this optional while others maintain this is good practice

- Repeat this process until the pack reforms
- “Pack” or “Pack is here” is announced when the pack is reestablished.
