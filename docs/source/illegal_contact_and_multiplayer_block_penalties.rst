Lesson #20: Illegal contact and multiplayer block penalties
===========================================================

*by Stephen Lorimor / Axis of Stevil*

Watch `lesson #20a (Illegal contact penalties) <https://www.youtube.com/watch?v=TtM_agEfe6s&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7&index=23>`_ and `lesson 20b (Multiplayer block penalties) <https://www.youtube.com/watch?v=VoTkhmjy1cc&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7&index=24>`_.

Last updated: January 9, 2019


Concepts
--------

- A skater’s direction is determined by the motion of their skates.

   - If the skates are moving in opposite directions, use the hips instead.

- Blocks initiated while moving perpendicular to the track lines are legal
- Illegal contact while a jam is in progress is always a penalty if the block causes...

   - the target to fall down or go out of bounds, out of play, or back into play
   - the target to lose position against the initiator or an initiator’s teammate
   - the initiator or a teammate to gain position on an opponent

      - Brief and minor gains of position do not warrant a penalty unless earning a pass toward scoring or lead jammer status


4.1.3 - Other illegal contact
-----------------------------

- Illegal blocks/assists unrelated to target zones and blocking zones.
- Verbal cues

   - “Illegal contact”, or

      - “Illegal assist”
      - “Early hit”
      - “Late hit”
      - “Out of play block”

   - “Direction”, or

      - “Stop block”

- Early hitting

   - Initiating a block before the jam start whistle
   - Penalty thresholds

      - “Stealing” an opponent’s starting position
      - Forcing the opponent to touch an illegal starting position

- Jammers touching the pivot line, behind the pivot line, or ahead of the jammer line
- Blockers touching the pivot or jammer lines, ahead of the pivot line, or behind the jammer line
- Pivots touching on or behind the jammer line, or ahead of the pivot line
- Any skater going down or touching out of bounds
- Late hitting

   - Initiating or continuing a block after the fourth call-off whistle
   - Penalty thresholds

      - Forcing an opponent down or significantly off balance

- Blocking to or from out of play

   - Initiating a block while out of play or against an out of play target

      - Exception: jammer vs. jammer blocking is legal anywhere on the track
      - Exception: counter-blocking is legal while out of play

- Blocking while stopped or moving clockwise

   - Initiating or maintaining a block while stopped or moving clockwise
   - Additional penalty threshold

      - Severely changing an opponent’s speed or trajectory

- Exception: if the initiator immediately ceases the block or resumes counterclockwise movement
- Illegal assists

   - Assisting while down, out of play, stopped, skating clockwise, or fully out of bounds

      - Active assists while straddling are illegal; passive assists are legal

   - Penalty thresholds

      - Causing a change of relative position
      - Helping a downed skater upright in the engagement zone while stopped is considered good sportsmanship, not impact.

- Additional illegal contact

   - Blocking to or from out of bounds

      - Initiating a block against a fully out of bounds opponent
      - Initiating or continuing a block while out of bounds (including straddling)
      - Picking up speed for a block from out of bounds, even if the contact takes place once fully in bounds
      - Penalty threshold

- Significantly delaying a fully out of bounds skater’s return to the track

   - Blocking while down
   - Blocking a downed skater

      - Penalty threshold

- Delaying their return to the track

   - Blocking while airborne

      - The rules are contradictory whether this is illegal contact or misconduct
      - Penalty threshold

- Forcing an opponent out of their established position


4.1.4 - Multiplayer blocks
--------------------------

- Verbal cue: “Multiplayer”
- A type of illegal block in which two skaters form a wall (sometimes called a “link”) using one of these three techniques:

   - Grasping, linking, or impenetrable wall

- The multiplayer block must be physically challenged by an opponent

   - Exception: if the grasp or link that cannot be reached because it is ahead of the “wall” of both participants, and because it strengthens said “wall”

- Impact

   - Preventing the opponent from passing between the skaters

      - Usually counterclockwise, but can be perpendicular to the track (uncommon) or even clockwise (rare).

- Upon being challenged, immediately releasing a multiplayer block such that it prevents the opponent from being impeded is not a penalty.
- The penalty should be issued to the most responsible skater.

   - When in doubt, issue it to the closest skater involved in the block.

- (rare) Jammers can get multiplayer block penalties.
