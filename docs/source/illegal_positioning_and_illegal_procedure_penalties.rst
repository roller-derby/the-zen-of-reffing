Lesson #21: Illegal positioning and illegal procedure penalties
===============================================================

*by Stephen Lorimor / Axis of Stevil*

`Watch this lesson <https://www.youtube.com/watch?v=DVyEJg3ZkrA&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7&index=25>`_

Last updated: June 11, 2018


4.2.1 - Illegal positioning
---------------------------

- Illegal positioning penalties are for…

   - Adopting or maintaining a position in which one cannot be blocked (down, out of bounds, or out of play)
   - Destroying the pack
   - Starting the jam in an illegal starting position for their role.

- It is legal to gain an illegal position for reasons of safety.
- It is legal to maintain an illegal position because of the perception that re-entering would constitute an illegal act
- Types of illegal positioning penalties and their verbal cues.

   - “Illegal position” (the basic cue)

      - Starting the jam in a completely illegal starting position

- Exception: being blocked into an illegal position by an early hit

      - Assuming a downed position to deter an incoming block

   - “Destruction”

      - A sudden, marked, or abrupt action that destroys the pack for a reason other than normal gameplay.

- Some examples of normal gameplay

   - Passing the star
   - Initiating or continuing a block
   - Returning to the track, engagement zone, and/or an upright position
   - Receiving a block
   - Accidentally falling down or out of bounds
   - At jam start, all of a team’s skaters immediately and rapidly skate (in either direction) until the pack is destroyed

- Clockwise movement is held to a higher standard than counterclockwise movement.  It does not have to be sudden, marked, or abrupt.
- Pack destructions without impact are not penalized (“No pa--pack!”)
- (rare) Both teams can destroy the pack at the same time.  One penalty should be issued to the most responsible skater on each team.
- Jammers can never receive a destruction penalty.

   - “Skating out of bounds”

      - Exiting the track without a legal reason or the belief one has a legal reason

- Some legal reasons (there are many more than this)

   - Going out of bounds as a result a missed block or receiving a block
   - Exiting the track during the jam call-off whistles
   - Exiting the track to retrieve an out-of-bounds helmet cover
   - Exiting the track following a failed apex jump
   - Falling out of bounds by accident

      - Maintaining or increasing speed while out of bounds

- Exception: accelerating to re-enter the track
- Exception: accelerating to reach the penalty box

   - “Failure to reform”

      - After a warning, failing to act to reform the pack

- Penalty goes to the most responsible skater per team.

      - See lesson 25 (“No pack situations”)

   - “Failure to return”

      - After a warning, failing to return to the engagement zone

- Penalty goes to all offending skaters
- Standing in place is considered returning so long as the pack is approaching the skater.
- An out of play (front) blocker must skate clockwise if the pack is stopped or moving clockwise.

   - This is the only situation in the entire game when a skater must skate clockwise.
   - “Failure to yield”

      - After a warning, failing to yield after starting the jam while touching a illegal starting area for one’s role (ex; jammer touching past the jammer line)

- (rare) A warning is not necessary to issue a “failure to ____” penalty if the skater cannot reasonably believe they are legally positioned and/or cannot return to a legal position.

   - Example: a skater returns from the penalty box across the track from the engagement zone.  Instead of returning to the engagement zone, the skater then engages a jammer.


4.2.4 - Other illegal procedures
--------------------------------

- Technical infractions with significant impact, if not necessarily conferring an advantage to a specific team or skater
- When possible technical infractions should be resolved before they impact the game
- Types of illegal procedures and their verbal cues.

   - “Illegal procedure”

      - Removing safety gear other than a mouthguard while in the penalty box
      - Leaving the penalty box before one’s time is expired

- Exception: when permitted to leave by an official
- Improper use of safety gear, skates, uniform, or jewelry

      - Illegally controlling a helmet cover
      - Careless actions (ex; throwing a water bottle to another skater) that threaten the safety of an official, but do not forcibly contact them.
      - A captain or alternate successfully requesting a timeout or official review while not wearing the C or A

   - “Star pass violation”

      - Illegally passing the star

   - “Pass interference”

      - Preventing a star pass via an illegal action

- Verbal cues

   - “Illegal procedure”, or

      - “Star pass violation”
      - “Pass interference”
