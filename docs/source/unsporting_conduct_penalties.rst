Lesson #23: Unsporting conduct penalties
========================================

*by Stephen Lorimor / Axis of Stevil*

`Watch this lesson on YouTube <https://www.youtube.com/watch?v=lSDWeSnc640&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7&index=28>`_

Last updated: January 9, 2019


4.3 - Unsporting Conduct
------------------------

- This penalty type involves significant officiating discretion, and is not limited to actions listed on this handout.
- Unsporting conduct penalties can be elevated to expulsion for egregious or repeated offenses depending on the context.
- Unsporting conduct penalties and their verbal cues

   - “Misconduct” (basic cue) - Undermining the legitimacy of the sport or displaying a lack of respect for the sport, its execution, or those that contribute to it.

      - Abusive or obscene language or gestures directed at a non-official

- (no call or warning) Minor slights inaudible to the audience addressed to a teammate or opponent
- (expulsion) Long strings of profanity or losing one’s temper

      - Impersonating an official
      - Hiding an active helmet cover during a jam
      - Reckless or negligent penalty box entry
      - Attempting to deceive a referee into issuing a penalty
      - Blocking outside the acceptable range of the sport

- Positional blocking preventing a downed skater from standing
- Blocking while airborne

   - Non-forceful / unintentional contact with significant impact
   - Intentional contact regardless of impact

- (expulsion) Intentionally falling on a prone or down skater
- (expulsion) Pulling a helmet cover off an opponent

      - Failing to abide by governing body policies during the game

- Example: fielding too many team staff

      - Intentionally blocking a skater into a downed skater
      - An ejected skater that interferes with subsequent gameplay

- Penalty to captain, skater is banished to the locker room

      - (expulsion) Intentional, reasonably avoidable, and forceful contact to an official

   - “Insubordination” - Willfully or neglectfully failing to comply with the instructions of an official, or wrongful or improper behavior motivated by intentional disregard for the rules.

      - Abusive or obscene language or gestures directed at an official

- Not every slight warrants a penalty

      - Willfully failing to follow the instructions of an official

- This includes immediately leaving the track in response to a penalty

   - A skater must exit the track relative to where they were issued the penalty.
   - A bit ahead or behind this area has no impact.
   - A lot ahead or behind has impact.
   - Exiting is also subject to having a safe and legal opportunity to exit the track.

      - Failure to leave the track after repeatedly being directed to do so
