.. Zen of Reffing documentation master file, created by
   sphinx-quickstart on Thu Mar 30 13:46:06 2023.


Welcome to the Zen of Reffing Roller Derby!
===========================================



A training manual for referees
------------------------------

Created by Stephen “Axis of Stevil” Lorimor. Maintained by Isabelle “Blocktopus” Santos.

**Bookmark this URL:** http://zen-roller-derby.xyz

`Get printable version here <https://roller-derby.frama.io/the-zen-of-reffing/zen-roller-derby.pdf>`_

Last updated: September 2023.

**YouTube videos on Illegal Target Zones and Passing the Star are in need of updating.**


As used by
..........

.. list-table:: 
   :widths: 50 50
   :header-rows: 0
		 
   * - Badlands Roller Derby League (Drumheller, Canada)
     - Montreal Roller Derby (Montreal, Canada)
   * - Boom Town Roller Derby (Mat-Su Valley, Alaska)
     - New Hampshire Roller Derby (Manchester, NH)
   * - Brazos Valley Roller Derby (Bryan, Texas)
     - New Jersey Roller Derby (Morristown, New Jersey)
   * - Brighton Rockers Roller Derby (Brighton, UK)
     - North Texas Fallout Junior Roller Derby (Denton, Texas)
   * - Capital City Crushers (Topeka, Kansas)
     - North Texas Roller Derby (Denton, Texas)
   * - Central California Area Derby (Fresno, California)
     - Old Capitol City Roller Derby (Iowa City, Iowa)
   * - Central West Roller Derby League (Bathurst, Australia)
     - Pensacola Roller Gurlz (Pensacola, Florida)
   * - Charm City Roller Girls (Baltimore, Maryland)
     - Pirate City Rollers (Auckland, New Zealand)
   * - Chattanooga Roller Girls (Chattanooga, Tennessee)
     - RockyView Roller Derby (Airdrie, Alberta Canada)
   * - Chick Whips Junior Roller Derby (Topeka, Kansas)
     - Roller Derby Iceland (Reykjavík, Iceland)
   * - Copper State Roller Derby (Tucson, Arizona)
     - Rollerderby Hanover (Hanover, Germany)
   * - Dragon City Roller Derby (Bendigo, Australia)
     - Sailor City Roller Derby (Buenos Aires, Argentina)
   * - Free State Roller Derby (Rockville, Maryland)
     - San Marcos River Rollers (San Marcos, Texas)
   * - Flint Roller Derby (Flint, Michigan)
     - Sintral Valley Derby Girls (Modesto, California)
   * - Gallatin Roller Derby (Bozeman, Montana)
     - Sirens of Smash Roller Derby (Nelson, New Zealand)
   * - Garden State Rollergirls (Newark, New Jersey)
     - Southern Oregon Derby (Medford, Oregon)
   * - Hellions of Troy Roller Derby (Troy, New York)
     - State College Area Roller Derby (State College, PA)
   * - Hurricane Alley Roller Derby (Corpus Christi, Texas)
     - Strong Island Roller Derby (East Setauket, New York)
   * - Ithaca League of Women Rollers (Ithaca, New York)
     - Suburbia Roller Derby (Yonkers, New York)
   * - Jerzey Derby Brigade (Morristown, New Jersey)
     - Tenerife Roller Derby (Tenerife, Spain)
   * - Les Quads de Paris (Paris, France)
     - Tokyo Roller Girls (Tokyo, Japan)
   * - Lille Roller Girls (Lille, France)
     - Veuves Noires de Rouen (Rouen, France)
   * - Molly Rogers Rollergirls (Melbourne, Florida)
     - Whenua Fatales Roller Derby (Levin, New Zealand)
   * - Roller Derby Toulouse (Toulouse, France)
     - 

Does your league use (or no longer use) this training manual? Please e-mail me so I can update this list.

----

*Permission is granted to share this document and to adapt and share modified versions provided appropriate credit is given to the original author, and you indicate what, if any, changes were made. Please see the* `Creative Commons License <https://creativecommons.org/licenses/by-nc-sa/4.0/>`_ *for more information.*

**The Zen of Reffing Roller Derby**

© Stephen (“Axis of Stevil”)

About these handouts
....................

These handouts were written by Stephen (“Axis of Stevil”) Lorimor. This program is his own creation and he is solely responsible for its content.


About the author
................

Axis of Stevil, a self-described rules nerd, is a former WFTDA level 1 certified referee and a member of the WFTDA Rules Committee.  He is dual-affiliated with both Garden State Rollergirls and Jerzey Derby Brigade. He formerly volunteered with the WFTDA Apprentice Program and served as lead writer for Roller Derby Rule of the Day on Facebook. He is also the former co-THR for the JRDA Champs and JRDA World Cup. The truly curious can read his game history at www.tinyurl.com/stevil2. In his downtime he’s the webmaster of Roller Derby Patches, the owner of Patches by Stevil, and admin of the Roller Derby Patches group on Facebook.

About the Maintainer
....................

Blocktopus has been swallowed by an all-engulfing passion for the rules of roller derby. She is the author of tools such as `the random penalty generator <https://roller-derby.frama.io/-/random-penalty-generator/-/jobs/2092254/artifacts/public/index.html>`_, `roller derby inspiration <https://www.instagram.com/rollerderbyexpiration/>`_ and the "who's reffing?" game.


Ruleset
.......

This document is updated for WFTDA’s Rules of Flat Track Roller Derby (January 1, 2023 version). On occasion information is presented in light of rules discussions accessible to WFTDA member leagues via the WFTDA forums. No information contained in these handouts violates WFTDA’s non-disclosure agreement.


Special thanks
..............

The knowledgeable folks at `Zebra Huddle <www.zebrahuddle.com>`_ assisted me in peer reviewing many of the earlier handouts on referee procedures. I’d also like to thank the many readers who have taken the time to email their thoughts and observations on The Zen of Reffing Roller Derby. Finally, I’d also like to thank the WFTDA, MRDA, and JRDA and their volunteers for their tireless dedication to the sport of roller derby.


Feedback
........

Readers are encouraged to submit feedback, corrections, criticisms, and comments via email at njderbyref@gmail.com or via `Stephen’s page on Facebook <https://m.facebook.com/profile.php/?id=1099311093>`_. Reports of typos, bugs, erroneous information, and the like are always welcome.


Disclaimer
..........

The WFTDA, MRDA, or JRDA are not responsible for this document and make no claims as to the accuracy of its content.


Contents
--------

.. toctree::
   :maxdepth: 1
   
   duties_skills_and_ethics
   whistles
   verbal_cues_and_hand_signals
   game_concepts
   safety_gear_skates_and_accessories
   safety_injury_bleeding_and_concussion
   referees_all_positions
   outside_pack_referees
   inside_pack_referees
   jammer_referees
   the_head_referee
   the_penalty_box
   skater_concerns_and_official_reviews
   pack_formation
   basic_scoring
   advanced_scoring
   introduction_to_penalties
   three_types_of_penalties
   illegal_target_and_blocking_zone_penalties
   illegal_contact_and_multiplayer_block_penalties
   illegal_positioning_and_illegal_procedure_penalties
   cutting_the_track_and_altering_game_flow_penalties
   unsporting_conduct_penalties
   expulsions_and_fouling_out
   no_pack_situations
   assists
   clockwise_play
   passing_the_star
   junior_derby_gameplay
   junior_derby_gear_checks
   document_change_summary
   

All lessons have accompanying videos available on YouTube. 
The entire set of video lessons can be found at this `link <https://www.youtube.com/playlist?list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7>`_.



* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
