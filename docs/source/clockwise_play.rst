Lesson #27: Clockwise play
==========================

*by Stephen Lorimor / Axis of Stevil*

`Watch this lesson on YouTube <https://www.youtube.com/watch?v=joyLM0_63Yw&index=32&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7&t=0s>`_

Last updated: January 21, 2018


General
-------

- Although roller derby is played counterclockwise, it is legal to skate clockwise.

   - Exception: a rear group reforming the pack during a no pack situation
   - Exception: out of play blockers to the rear of the engagement zone

- The direction a skater is facing while skating clockwise is irrelevant.
- Skaters are never required to skate clockwise

   - Exception: out of play blockers to the front of the engagement zone when the pack is stopped or moving clockwise.


Clockwise jammers
-----------------

- Jammers rarely skate clockwise except to avoid track cuts.
- There is no “running lap count” for jammers.

   - A jammer that falls behind can be absorbed into the prior trip through the pack.

      - No missed points or credit toward lead jammer can be obtained

   - No matter how many times a jammer is lapped by the pack, the jammer needs only pass through the pack and exit the front of the engagement zone to return to the current lap.

- Eating the baby

   - Jammer A knocks Jammer B out of bounds at the start of the jam, after which Jammer A skates clockwise to the front of the pack. Jammer B must enter behind them, which is behind the pack of their prior lap. Jammer A then skates counterclockwise on their regular initial trip. Jammer B must effectively pass through the pack twice to complete their initial trip.


Clockwise pack movement
-----------------------

- The game does not change directions when the pack travels clockwise

   - The front and rear of the pack do not switch sides
   - Jammers must still pass skaters in the counterclockwise direction to earn passes on opposing blockers
   - Clockwise-moving skaters cannot legally initiate blocks or assists
   - Counterclockwise skating jammers can legally initiate blocks against clockwise skaters.

- Cutting the track and track re-entry rules are not altered.

   - Skaters must still re-enter behind (ie; clockwise) skaters to avoid illegally gaining position

- Out of play rules regarding no pack situations do not change

   - The rear group must still skate counterclockwise to reform
   - The front group can stop their clockwise motion as fast as they want and remain still until the counterclockwise group reaches them
   - Destruction penalties are assessed as normal. If the team makes a sudden, marked, and rapid stop or change of direction (including back to counterclockwise), it should be met with a penalty
