Document change summary
=======================

This is a list of changes within the last six months. It does not include minor fixes, tweaks, corrections, video updates, etc.

------------

April 2023 - Update for the 2023 changes on counterblocking.

May 19, 2022 - A slew of minor grammatical fixes suggested by Molotov Latte.  (Thanks!)

November 26, 2019 - Updated the link in Lesson 29 (“Junior derby - gameplay”) to the current JRDA rule set.

January 14, 2019 - Corrected a typo which significantly changed the meaning of the new metric for a back block in Lesson 19 (“Illegal target and blocking zone penalties”).

January 9, 2019 - Modified numerous lessons to bring them up to date for the 2019 rule set. Removed specific casebook citations as casebook numbers have changed.

August 30, 2018 - Added lesson 30 (“Junior derby - gear checks”).

August 3, 2018 - Revised lesson 28 (“Passing the star”) for better readability and comprehension.
