Lesson #12: The penalty box
===========================

*by Stephen Lorimor / Axis of Stevil*

`Watch this lesson on YouTube <https://www.youtube.com/watch?v=fDufLoOIKhA&t=0s&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7&index=14>`_

Last updated: May 25, 2018


The basics
----------

- This lesson does not cover box staff verbal cues; speak to an experienced official for more information.
- The penalty box consists of two sets of three seats

   - Two seats are for jammers
   - The other four seats are for blockers (two seats for each team)

- The boundary line is considered part of the penalty box.

   - Touching in or on the line is considered “in the box”

- A penalty is 30 seconds long.  Two penalties warrant 60 seconds, etc.

   - An official must alert the box staff if multiple penalties are to be served.

- A skater begins serving time by sitting in any box seat.
- Penalty time…

   - … is only served while a jam is in progress.
   - … is only served while seated.

      - Exception: penalized skaters must stand for the last ten seconds

- When a skater stands with less than ten seconds of penalty time remaining, their seat becomes available for a new skater.
- A skater must exit the box once their time has ended.
- If both of a team’s blocker seats are occupied, incoming blockers from their team are waved off from the box

   - Waved-off blockers are now “in the penalty box queue” and may return to the track
   - The blocker must return to the box when a seat becomes available
   - The skater must enter all new jams in the same position until their penalty time is served.

- Timeouts and official reviews may not be called from the penalty box.
- Skaters in the penalty box or penalty box queue cannot be substituted during or between jams.

   - Substitutions are permitted between jams in case of injury, fouling out, major equipment failure, expulsion, etc.
   - The penalty is applied to the offender’s tally, but the remaining time is served by the substitute.

- If a jammer is in the penalty box and the other jammer…

   - … removes themself from play due to injury, the jam is called off
   - … quits the game / is expelled / is fouled out, the jam is called off
   - … exits the track due to equipment failure…

      - … the jam should be called off if the skater removes themselves from play due to major equipment failure (broken plate, lost wheels, etc.)
      - … the jam should not be called off if the jammer temporarily withdraws to fix a minor equipment issue (loose toestop, laces, velcro, etc.).
      - Officials should use their discretion to determine which solution best fits the situation.


Jammer swaps
------------
- When both jammers receive penalties, the first jammer is released when the second jammer is seated and the second jammer’s time is reduced by the unserved penalty time of the first.

   - This only works “per pair of penalties”

- Example 1: both jammers sit in the box at the same time

   - Both jammers are immediately released

- Example 2: one jammer sits followed by the second

   - Jammer A is released. Jammer B is must serve 30 seconds minus Jammer A’s unserved time.

- Example 3: same as #2, but Jammer A returns before B finishes their time.

   - Neither jammer is released.
   - Both jammers serve their remaining time

- Example 4: same as #3, but Jammer B returns before Jammer A finishes their time

   - Jammer A is released. Jammer B must serve 30 seconds minus Jammer A’s unserved time.

- Example 5: one jammer with two penalties sits in the box, followed by the second.

   - Jammer A’s penalty time is reduced by 30 seconds. Jammer B is released.


The penalty box whiteboard
--------------------------

- The box whiteboard is divided into labeled halves -- one for each team.
- A lone number means that skater has unserved time.
- A penalty code underneath a number means the skater has committed that penalty.

   - A circled code means the box has assessed the penalty.

      - This communication is for the penalty tracker, not the referees

   - An uncircled code means a referee must issue the penalty.


Related penalties
-----------------

- Delay of game

   - Failing to enter the next jam in the correct position while in the penalty box queue

- Illegal positioning

   - Remaining in the penalty box after being released by the penalty box staff and ignoring a subsequent warning to return to play.

- Illegal procedure

   - Removing safety equipment while in the penalty box

      - A mouthguard may be removed while seated, but must be replaced prior to exiting

   - Exiting the penalty box before one’s time has expired
   - Fully entering the penalty box to communicate with a penalized skater
   - Fully entering the penalty box while not penalized then interfering with normal penalty box operation

- Misconduct

   - Hot entry into the penalty box that contacts another skater or official

      - Significant discretion is given regarding how this is enforced
      - Mitigating circumstances (ex; water on the track) may be considered
      - Forceful and reasonably avoidable contact to a penalty box official, including contact from a box seat, warrants expulsion.
