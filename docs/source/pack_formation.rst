Lesson #14: Pack formation
==========================

*by Stephen Lorimor / Axis of Stevil*

`Watch this lesson on YouTube <https://www.youtube.com/watch?v=Smgn9sWBX1Q&index=16&t=0s&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7>`_

Last updated: May 27, 2018


Concepts
--------

- The pack is a fundamental aspect of game play
- Its location determines the engagement zone where most blocking takes place
- A blocker that exits the engagement zone is warned to return.

   - Failing to return warrants a penalty.

- Intentionally destroying the pack can be a penalty.
- The nonexistence of a pack (a “no pack” situation) triggers a set of strict rules for blockers to re-establish its existence

   - Failing to reform the pack warrants a penalty.

- The location of an individual skater is measured at the centerpoint between their hips.


Definitions
-----------

- Pack - The largest group of upright, in bounds blockers skating or standing in close proximity (10’ links) containing members from both teams.

   - “Largest” means two or more equal-sized groups cannot constitute a pack.
   - “Upright, in bounds” means down or out of bounds skaters are not part of the pack.  

      - This includes skaters straddling the line.

   - “Blockers” means Jammers are not part of the pack.
   - “Close proximity” is measured at the centerpoint of the track. The skater needs to be mentally “slid” to the center of the in-bounds area of track. Ten feet is then measured forward and backward from that point.
   - “Members of both teams” means a group with only one team present cannot be a pack.

- Engagement zone - An area stretching from 20 feet in front of the pack to 20 feet behind it.

   - Blocking is illegal outside of this zone.

      - Exception: jammer vs. jammer blocking is legal.

- In play - The status of any blocker who is upright and in bounds within the engagement zone.

   - Jammers who are upright and in bounds are in play. They need not be in the engagement zone.

- Out of play - The status of any blocker who is upright and in bounds, but not inside the engagement zone.

   - If there is no engagement zone, all blockers are out of play.

- No pack - When no group of blockers meets the definition of a pack
- Split pack - A type of “no pack” situation wherein there are two or more equally valid packs.


Homework
--------

- *After going over this lesson, I strongly suggest going over the pack formation exercise packet available in the Zebra Huddle files section. It is an exceedingly useful assignment for a ref trainee learning about pack formation. (Approximate time: 15 minutes)*
