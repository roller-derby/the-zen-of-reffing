Lesson #22: Cutting the track and altering game flow penalties
==============================================================

*by Stephen Lorimor / Axis of Stevil*

Watch `lesson #22a (Cutting the track) <https://www.youtube.com/watch?v=naH4UzJYlVA&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7&index=26>`_ and `lesson 22b (Altering game flow) <https://www.youtube.com/watch?v=B6JKoaw8O3Q&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7&index=27>`_.

Last updated: June 14, 2018


4.2.2 - Gaining position (“cutting the track”)
----------------------------------------------

- The illegal use of the out-of-bounds area to gain position on another skater.
- Concepts

   - Gains of positions are always measured at the hips
   - A downed skater who re-enters the track does not gain position until they are upright
   - Skaters have no position if they are down or out of bounds
   - Blockers have no position if they are outside an existing engagement zone
   - No pack situations do not exempt skaters from illegal gains of position

      - During a no pack situation, blockers more than 20 feet from the last defined pack have no position.

   - Illegal gains of position may be “ceded” by immediately and fully exiting the track

      - Signaling jam call-off does not negate a skater’s obligation to cede, although the jam may end before the skater is able to do so.
      - Officiating discretion is sometimes necessary to determine whether a skater is ceding.

- Impact to call a penalty

   - A skater bettering their position on one or more opponents
   - A skater bettering their position on two or more teammates

- Gaining position penalties and their verbal cues

   - “Cut” (basic verbal cue)

      - Refers to a skater returning from out of bounds (including straddling)

- Does not include the penalty box, or temporary withdrawal from the jam due to equipment failure or injury

      - A skater who is forced out of bounds must return behind all opponents who had superior position when they went out of bounds.
      - All upright, in bounds skaters ahead of the skater who went out of bounds have superior position.

- Superior position is temporarily lost if the skater goes down, out of bounds, or out of play, but can be regained by reversing this.

      - The initiator of a block that forces a skater out of bounds gains superior position, even if they were physically behind the target.

- This privilege comes with a price; superior position is permanently lost if they go down, out of bounds, or out of play.

      - All skaters passed during an apex jump that lands in a straddling position retain superior position, even if points were scored.

   - “Illegal re-entry”

      - Refers to a skater returning from the penalty box or temporary withdrawal from the jam due to equipment failure or injury
      - All in-play blockers have superior position.

- Jammers do not have superior position.


4.2.3 - Altering the flow of the game
-------------------------------------

- Warnings should be used whenever possible to fix a situation instead of issuing a penalty

   - The larger objective is to maintain game flow, not to send more skaters to the box

- Altering game flow penalties and their verbal cues

   - WFTDA has not formalized the difference between these penalty types, but these are generally acceptable distinctions
   - “Interference” (basic cue)

      - Illegal actions that interfere with the natural progression of the game, but do not halt the game clock

- Successfully calling off the jam while not lead jammer

   - Exception: if an official erroneously indicates the jammer has lead

- Too many skaters achieving a competitive advantage during a jam

   - “Delay of game”

      - Illegal actions that cause the game clock to be halted

- Failing to field a jammer at jam start
- Failing to field any blockers in a legal starting position at jam start
- Failing to field a blocker in the penalty box queue in the correct position at jam start
- An injured skater lined up for a jam at jam start for which they are not allowed to participate
- Successfully requesting a timeout (TO) or official review (OR) with none of that type remaining

   - If possible, charge the team the other type instead of issuing a penalty

- Successfully requesting a TO or OR by a non-captain/designated alternate

   - Exception: Calling a TO/OR in response to a skater signaling to their captain or alternate to request a TO/OR.
   - Docking a TO or OR instead of issuing a penalty is not an option.

- Penalties for altering game flow go to the team captain

   - Exceptions:  (penalty to the responsible skater)

      - Too many skaters
      - Failing to field a skater in the penalty box queue in the correct position
      - Calling off the jam while not lead jammer
      - Successfully calling for a TO/OR by a non-captain/designated alternate
