Lesson #30: Junior derby (gear checks)
======================================

*by Stephen Lorimor / Axis of Stevil*

Last updated: August 30, 2018


Full procedure instructions can be found in the `JRDA Pre-Game Protocol <https://cdn1.sportngin.com/attachments/document/0112/0200/JRDA_Pre-Game_Protocol.pdf>`_.


Gear Checks
-----------

- All junior skaters must undergo a physical “gear check” before regulation and sanctioned games.

   - The gear check is either provided by officials (supervised by team staff), or by team staff (supervised by officials).  This is typically the captains’ choice, to be decided at the captains’ meeting.
   - Checks involve making physical contact with skaters and their gear.
   - The check should be performed in public with a team staff member nearby.
   - Skaters must grant consent before being checked.

      - A skater may decline to be checked, but is not cleared to skate in the game.
      - To avoid misunderstanding, explain the check as it is being performed.

   - Officials and team staff must also consent to participate in a check.

      - No official or team staff should ever be forced to perform checks.

   - NSOs who understand how to perform a gear check may perform them at the discretion of the head referee.
   - Skaters are not cleared to skate in the game until they pass their gear check.

      - Issues noticed during gear checks must be resolved prior to the skater participating in the game.  This may be after the first whistle.
      - All equipment issues that are not immediately resolved should be brought to the attention of the head referee.

- The head referee is the final authority on all safety gear, and may request any item be modified, removed, and/or replaced.
- Manufacturer instructions regarding sizing, use, and maintenance always take precedence if they differ from these guidelines.
- Brief summary of gear check procedures.

   - All gear

      - May not display any offensive words or symbols.
      - Must be in good repair.

- Hard shells and inserts may not be cracked.

   - Helmet

      - Should not be able to slide side-to-side or tilt forward or backward
      - Helmet should be low over the forehead with the front rim approximately at the eyebrows.
      - Chin strap should be snug but not impede breathing and movement of the mouth, and should allow a maximum of two fingers to fit between the strap and chin.
      - The helmet should not have any cracks or sharp edges.
      - Face shields are allowed / disallowed as per the WFTDA Risk Management Guidelines.

   - Mouth guard

      - Ask the skater to display their mouth guard.
      - A mouth guard may be trimmed for comfort / fit, but should not be shortened or cut off before the molars.
      - The mouth guard should fit when they place it back in their mouth.

   - Elbow pads / knee pads

      - Should be snug and not slide on their arm/leg.
      - Must have a hard shell that is not cracked or broken.
      - Tape should be used to secure loose velcro.

   - Wrist guards

      - Should be snug on their wrist and not able to be slid in any direction.
      - Must have a plastic shell or insert that is not cracked.

   - Skates

      - Skates are not considered safety equipment and are not part of the gear check.
      - Toe stops are not required on skates.

   - Other apparel

      - Jewelry, clothing, or other items may not have sharp or pointed edges that present a danger to other skaters.
