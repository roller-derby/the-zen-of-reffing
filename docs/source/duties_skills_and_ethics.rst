Lesson #1: Duties, skills, and ethics
=====================================
*by Stephen Lorimor / Axis of Stevil*

`Watch this lesson on YouTube <https://www.youtube.com/watch?v=241NQYHZSsc&t=1s&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7&index=1>`_

Last updated: January 21, 2018

Referee duties during scrimmages and games
------------------------------------------

- (top priority) Maintain safety
- Protect game flow
- Penalize illegally gained advantage
- Establish an atmosphere of professionalism, impartiality, and fairness


Referee duties (in general)
---------------------------

- Teach the rules to others
- Supervise or assist track setup
- Officiate drills and scrimmages
- Continue one’s own professional development
- Mentor newer referees
- Have fun!


Required skills for referees
----------------------------

- Skating skills
- Rules mastery
- The ability to correctly spot, process, and issue penalties
  
   - This can take years to develop.
   - Scrimmage experience is an important part of developing this skill
    

Conduct
-------

- Display good judgment in regards to behavior, honesty, fairness, and integrity
- Treat league and community members with respect, dignity, and fairness
- Maintain exemplary (even over-the-top) standards of neutrality and impartiality
  
   - No clapping, displaying team logos, posing for pictures with skaters, etc.
   - No hugs, high-fives, celebratory behavior, etc.
     
- Demonstrate a consistently high commitment to safety

   - We lose the authority to enforce safety in others if our own behavior is lax or inconsistent
     
- Discuss rules and procedural disagreements privately, not in front of others. The more formal/public the atmosphere, the more likely discretion should be exercised.
- Avoid conflicts of interest. Disclose all that exist to the head referee and/or crew
  
   - Avoid: gambling, public predictions, public opinions of skaters (coaches, etc.)
   - Allowed but disclose: Familial, financial, and associational
     
- Refrain from gratuitous displays of attention-seeking behavior while on duty.
- Zero tolerance of alcohol or drugs at derby-related events while officiating and/or on skates
- Be wary what you post on social media about the sport
- Always be respectful of NSOs -- they are equals, not inferiors.
