Lesson #6: Safety, injury, bleeding, and concussion
===================================================

*by Stephen Lorimor / Axis of Stevil*

`Watch this lesson on YouTube <https://www.youtube.com/watch?v=rK2ekwH8WO4&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7&t=0s&index=9>`_

Last updated: June 12, 2018


This is a simplified list of safety practices. See the WFTDA Risk Management Guidelines for more information.


Safety
------

- All games must have two dedicated emergency medical providers.
  
   - They may not have a secondary job in the game (skater, NSO, etc.)
   - At least one must be watching the track at both times. Gameplay must halt if both are attending to a patient until at least one can resume watching the game.
     
- The vast majority of games must comply with the WFTDA Risk Management Guidelines
  
   - The outside safety lane must be 10 feet wide (5 with a 3-foot high raised fixed barrier)
   - Columns / posts (even padded) may not be within 5 feet of the inside track boundary or 10 feet from the outside safety boundary
   - I strongly encourage officials refuse to work games (and skaters to play games) in venues that fail to meet minimum safety standards.
     
      - This includes walking out of a game before it starts.
	
   - Some non-sanctioned games outside the USA played under other insurance company safety guidelines may have different requirements.
     
      - Discuss this with your league if it applies to you.
	
- Safety gear should be worn at all times while standing on skates (see lesson #5).
- Skaters, refs, and NSOs may not participate in a game while under the influence of alcohol or narcotics.
  
   - Immediately report even suspected violations to the game head officials.


Injury
------

- Minor (no immediate danger to self or others)
  
   - Examples: broken finger, sprained knee, having the “wind knocked out of you”
   - Skaters may withdraw from a jam and/or resume play at their discretion
   - Skaters in the box may be subbed out between jams at their discretion
     
- Serious (potential danger to self or others, skater unable to continue)
  
   - Jam may be called at the ref’s discretion.
   - Skater unable to continue play should be directed off the track.
     
      - If fails to exit the track before active gameplay nears the skater, the jam may be called at the referee’s discretion.
	
   - Skater should be brought to the medics (or vice-versa) as appropriate
     
- Major (immediate danger to oneself or others)
  
   - Jam must immediately be called
   - Medics should be summoned to the skater
     
- Impaired / confused skaters
  
   - Jam may be called at the referee’s discretion
   - Speak directly to the skater
     
      - Tip: Ask the skater, “can you continue?” and not “are you okay?”, as skaters will often say they’re okay even when they are not.
      - If the skater does not respond or is disoriented, treat as a major injury.


Bleeding
--------

- A Skater who is bleeding must be removed from play
  
   - The skater may return to gameplay (even in the same jam) if these conditions are all met:
     
      - The bleeding has stopped
      - The wound is appropriately dressed
      - There is no visible blood on their gear and uniform
	
- Blood on uniforms must be washed and disinfected, covered with duct tape, or cut out
- Blood on gear must be cleaned and disinfected
- Call the jam if…
  
   - … the skater has significant bleeding
   - … there is blood on the track, safety lanes, penalty box, etc. -- even a single drop
      - Gameplay may not resume until affected areas are cleaned and disinfected


Concussion
----------

- A concussion is a traumatic brain injury cause by a shake, blow, or jolt to the head.
- A skater who is potentially concussed is no longer able to consent to playing in a game.
  
   - The skater must be cleared by the medics before they are allowed to return to play
   - A skater may refuse the medics’ evaluation, but is barred from participating for the remainder of the game
   - A skater whom the medics determine has no signs or symptoms of concussion may return to play
     
      - If this occurred in the first half of the game, the skater must be re-evaluated before the start of the second half.
      - If this occurred in the second half of the game, the skater must be re-evaluated at the end of the game.
	
   - Medics may request skaters return for re-evaluation at later times as well.
     
      - Example: before the skater’s next game of a multi-game event.
	
- Common symptoms of concussion include blacking out, headaches, abnormal behavior, confusion/disorientation, difficulty balancing, nausea, agitation, and sensitivity to light.
- For more information, see the CDC’s information program on concussions in youth sports at https://www.cdc.gov/headsup/youthsports/training/index.html.
