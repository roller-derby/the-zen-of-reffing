Lesson #19: Illegal target and blocking zone penalties
======================================================

*by Stephen Lorimor / Axis of Stevil*

`Watch this lesson on YouTube <https://www.youtube.com/watch?v=VXH3kvlPPK4&index=21&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7>`_

Last updated: January 14, 2019


Concepts
--------

- Contact with an illegal blocking zone or to an illegal target zone is always a penalty if the block causes...

   - the target to fall down or go out of bounds, out of play, or back into play
   - the target to lose position against the initiator or an initiator’s teammate
   - the initiator or a teammate to gain position on an opponent, or maintain a position that would not have been otherwise

      - Brief and minor gains of position do not warrant a penalty unless earning a pass toward scoring or lead jammer status

- It is not a penalty if the action was caused by the illegal action of an opponent


4.1.1 - Impact to an illegal target zone
----------------------------------------

- Verbal cues and their corresponding illegal target zones

   - “Low block” - legs below mid-thigh
   - “Back block” - butt or back (colloquially “between the bra straps”)
   - “High block” - neck or head

- An illegal target zone block becomes legal (ie; the target becomes the initiator) if the target turns or moves thereby presenting an illegal target zone that cannot be avoided.
- Contact with an opponent’s legs after falling small is not a penalty.

   - Exception: if this occurs 3+ times in a game
   - Exception: if sliding back in bounds initiates the contact

- Avoidable forceful contact to the back warrants a penalty regardless of impact.
- Forceful contact to the neck or head warrants a penalty


4.1.2 - Impact with an illegal blocking zone
--------------------------------------------

- Verbal cues and their corresponding illegal blocking zones

   - “Leg block” - legs (below mid-thigh)
   - “Forearm” - arms (elbow to fingertips) not tucked into the torso
   - “Head block” - head or neck

- Additional penalty thresholds include:

   - Significantly altering an opponent’s speed (ie; impeding) or trajectory
   - Forcing an opponent significantly off balance
   - Intentionally and forcefully jabbing an opponent with the elbow or knee

- Head blocks need not include physical blocking (ie; can be positional) to warrant a penalty

   - Intentional positional blocking

      - No call - unintentional positioning causing an opponent to stop / change trajectory
      - Penalty - intentionally presenting one’s head to deter an opponent’s block
      - Penalty - continuing to deter an opponent’s block with one’s head after an initial unintentional positional block
      - Expulsion - reckless or negligent intentional positional block

   - Initiating a block with the head

      - Penalty - forceful contact initiated with the head or neck
