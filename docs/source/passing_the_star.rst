Lesson #28: Passing the star
============================

*by Stephen Lorimor / Axis of Stevil*

`Watch this lesson on YouTube <https://www.youtube.com/watch?v=HEtE7z4XeTk&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7&index=33>`_

Last updated: January 9, 2019


Concepts
--------

- A legal star pass transfers both the star (jammer helmet cover) and the position of jammer from the jammer to the pivot.

   - Lead jammer status is not transferred
   - The new jammer is ineligible to become lead
   - The jammer who initiated the star pass becomes a blocker
   - The position of pivot for that team ceases to exist for the remainder of the jam
   - The pivot cover is considered debris and may be discarded or removed from the track
   - A legal star pass cannot be reversed or undone.
   - If the star pass occurs after the jammer “fell back” into a prior trip through the pack, the new jammer begins on the prior trip.

- Star passes are judged for legality if and when the jammer releases the star into the pivot’s grasp.
- Fallen star covers may only be retrieved by the pivot or jammer
- If the pivot is not wearing the stripe, they are treated as a non-pivot blocker.
- Star passes may be prevented by legal blocking
- A jammer or pivot carrying the star in a partially-hidden manner (ex; balled in their hand) should be warned to “show the star”.
- A pivot holding the star may drop, hand, or throw the star back to the jammer.

   - This includes a penalized pivot inside the penalty box.

- A failed star pass is either incomplete or illegal.

   - The physical star is transferred, but not the position of jammer.


Legal star pass process
-----------------------

- The jammer hands the star to the pivot
- Once both skaters are grasping the star, the jammer releases it.
- Jammer status is transferred the instant the jammer releases the star.
- The new jammer resumes the trip through the pack where the former jammer left off
- Credit for earned passes from the current trip through the pack is also transferred


Incomplete star passes
----------------------

- An incomplete star pass is a non-penalty violation of the star pass procedure
- Can occur several ways

   - Pivot catches a star in mid-air
   - Pivot retrieves a fallen star from the track
   - Pivot is handed the star from a blocker or opposing skater

- Following an incomplete star pass, the pivot should always be warned they are not the jammer.
- To complete a legal star pass, the jammer need only re-grasp the cover at the same time as the pivot, then let go


Illegal star passes
-------------------

- An illegal star pass is a potential penalty-level violation of the star pass procedure
- Can occur several ways

   - A jammer handing the star to the pivot while either skater is down, out of bounds, or outside the engagement zone
   - A jammer handing the star to the pivot while either skater is on their way to the penalty box, or while the pivot is in the penalty box queue
   - A jammer handing the star to a pivot not wearing the stripe.
   - A jammer handing the star to a non-pivot blocker

- Following an illegal star pass to a pivot, always warn the pivot they are not the jammer
- Penalties go to the initiator of the pass. This is almost always the jammer.

   - Exception: A blocker wrests the star away from the jammer, the blocker is the initiator
   - Exception: A pivot, after being warned they are not the jammer, who places the star upon their helmet.


Impact required to issue a penalty
----------------------------------

- “Illegal procedure” (no specific cue)

   - A pivot or jammer who completely hiding the star cover (in one’s jersey, shorts, hand, etc.)
   - A pivot or jammer who fails to produce the cover after being warned to “show the star”

- “Star pass violation”

   - Passing the star to a non-pivot blocker if the blocker fails to immediately relinquish control of the star. No warning is required.

      - This also applies to blockers who retrieve a fallen star or catch one in the air.

   - A pivot who, after being warned they are not the jammer, places the star on their helmet.
   - After being warned they are not the jammer, a pivot wearing the star that fails to remove it.
   - Advanced concept: After an illegal star pass that was intentionally illegal, if the ensuing confusion from that action provides a significant advantage such as allowing the Jammer to escape the pack.

- “Pass interference”

   - Disrupting an opponent’s star pass with an illegal blocking zone
   - Disrupting an opponent’s star pass by contacting illegal target zone
   - Disrupting an opponent’s star pass via illegal contact (clockwise block, etc.)
   - Controlling an opposing team’s helmet cover (holding it, standing on it, etc.)

      - Exception: Accidentally obtaining the opposing star cover then immediately dropping it or handing it back to the opposing pivot or jammer

- Expulsion: Gross Misconduct (rare)

   - Throwing an opposing star into the audience, bench area, etc.
   - Intentionally removing the star from the opposing jammer’s head


Additional rules and procedures
-------------------------------

- Officials should use the “star pass complete” hand signal following a legal star pass.
- Following a legal star pass both participants lose superior position in regards to drawing cuts on opposing skaters
- A star pass across opposing blockers does not earn passes.
- If a legal star pass occurs at jam start past all opposing blockers followed by the new jammer exiting the front of the engagement zone, this still counts as completing the initial trip.

   - It doesn’t matter that the new jammer never earned a pass on any opposing blockers

- Following an incomplete or illegal star pass, jammer referees should maintain focus on their jammer. Communicate verbally to the the IPRs, and let them deal with the pivot and fallen cover.
