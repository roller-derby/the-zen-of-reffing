Lesson #29: Junior Derby Gameplay
=================================

*by Stephen Lorimor / Axis of Stevil*

`Watch this lesson on YouTube <https://www.youtube.com/watch?v=jWnePnRh2Hk&index=34&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7>`_

Last updated: June 14, 2018


Basic concepts
--------------

- Junior derby utilizes the WFTDA ruleset subject to modifications listed in the Junior Roller Derby Association Rules Addenda (.pdf format).

   - Other junior derby rulesets exist; this lesson does not discuss them.

- Games utilizing the JRDA ruleset can occur at three “levels”.

   - Level one is “no contact”.
   - Level two is “gentle initial contact” / “lean blocking” only.
   - Level three is “regular adult derby”. All JRDA sanctioned games occur at this level.

- Junior derby places a stronger emphasis than adult derby on safety, personal development, and fostering a supportive environment.

   - Incidents such as crying, emotional distress, or unwillingness to communicate may be treated as injuries at the official’s discretion.
   - For some games (non-sanctioned, level 1-2, games with younger skaters, etc.) it may not only acceptable, but appropriate for officials to provide assistance in a manner not normally found at adult games.

      - For these games the captains’  meeting is an excellent place to discuss the level of assistance to be provided by officials.

Examples

- A skater on the track is issued a penalty. The skater moves away from gameplay, but does not leave the track and appears confused. It would be appropriate to provide a more specific instruction to report to the penalty box.
- A skater loses their toestop and falls down. They crawl off the track into the middle of the outside safety lane. The skater appears upset and unsure what to do next. It may be appropriate to retrieve their toestop, direct them off the edge of the safety lane, and/or notify their bench they need assistance.


JRDA Rules Addenda
------------------

- JRDA-only “hitting” penalties utilize the “illegal contact” penalty code, verbal cue, and hand signal.
- Level one

   - Only positional blocking is allowed; intentional contact or applying force to an opponent warrants a penalty regardless of impact.

      - This does not include minor incidental contact with no effect.
      - Unintentional contact that is otherwise legal (ie; legal target zone on legal blocking zone) is not penalized regardless of impact.

   - It is legal to adopt a downed position or go out of bounds to avoid contact with an opponent, even if contact could be avoided without doing so.

      - No penalty is issued for such an action even if it also destroys the pack.

- Level two

   - Forceful initial contact warrants a penalty regardless of impact or effect.

      - Follow-up contact is treated as a new block.

Example: gentle hip-to-hip contact followed by forceful shoulder contact warrants a penalty.

   - After legal and gentle initial contact is made, blockers may apply force in the same manner (ex; hip-to-hip, shoulder-to-chest, etc.).
   - Acceleration is irrelevant.  (This was an important issue in prior rule sets.)

- Level three

   - Contact penalties are treated in the same manner as adult derby.

- All levels

   - All non-championship tournament games may end in a tie.

      - Overtime jam(s) may be held if both teams agree. This may be repeated if the tie continues.

   - All non-gameplay behaviors that taunt, harass, or intimidate another person (skater, official, or anyone else) warrant a Misconduct penalty.

      - After a spectator (including parents) engages in one incident of such behavior, an official timeout should be called. Each captain should be given the opportunity to serve a penalty for the spectator’s conduct. If neither captain agrees, the spectator will be expelled from the venue.

- A second instance of Unsporting Conduct by the spectator automatically results in their expulsion from the venue.

   - The following behaviors warrant immediate expulsion on a first offense.

      - Repeated, excessive, physical dangerous, and/or egregious Unsporting Conduct.
      - Name-calling
      - Slurs based on race, religion, sexual orientation, etc.
      - Threatening a person’s or group’s health or well-being in any way.

Examples: 

- “I’m going to get you for that.”
- “Kill the jammer!”

      - Failure to remove or cover offensive language or symbols on uniforms or equipment after being instructed to do so by an Official.

   - Junior-specific expulsion procedures

      - Expelled skaters cannot be forced to leave the gameplay area without supervision.
      - Expelled skaters may remain in the audience with a designated supervising adult (to be identified in the pre-game meeting).
