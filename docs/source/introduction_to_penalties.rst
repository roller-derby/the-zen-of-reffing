Lesson #17: Introduction to penalties
=====================================

*by Stephen Lorimor / Axis of Stevil and Isabelle Santos / Blocktopus*

`Watch this lesson on YouTube <https://www.youtube.com/watch?v=joDMFs7KLVc&index=19&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7>`_

Last updated: March 31, 2023


Concepts
--------

- Penalties are a punishment for infractions related to:

   - Illegally gained advantage (for the individual or their team)
   - Disrupting game flow
   - Risking the safety of oneself or others
   - Unsporting conduct

- Three requirements to issue a penalty:

   - Initiator

      - Who initiated the action?

   - Nature

      - What was the status of the skaters involved? (ex; down, between jams, in bounds, entering the penalty box, etc.)
      - What parts of the skaters’ bodies were involved?
      - Was the action intentional?
      - Did the action appear egregious, reckless, and/or negligent?

   - Impact

      - What was the result of the action?

- The initiator of a block is always responsible for the legality of the contact

   - Exception: when the contact was caused by an opponent’s illegal action.

      - Example: White Jammer back blocks Black Blocker. Black Blocker falls into the legs of White Blocker causing them to fall as well. White Jammer receives a penalty; Black Blocker does not.

   - The initiator of the block isn’t always obvious (ex: back block vs. blocking with one’s back)

- Counter-blocking is held to the same standards as blocking

   - Counter-blocking is movement toward an incoming block to counter its force.

      - Exception: blocking while out of play. (Blocking is illegal, counter-blocking is legal)
      - Continued engagement after a counter-block becomes a block.

- A skater penalized between jams serves the penalty in the position they appear to be acting.


Definitions
-----------

- Relative position

   - A skater’s location on the track in relation to other skaters when that skater is in bounds and upright.
   - Relative position is judged based on the position of everyone involved in an engagement, not just the people who make contact.

      - Example: The relative position of a jammer that illegally blocks the left-most person of a three-person wall is judged against everyone in that wall.

- Advantage

   - An event that benefits one skater or team over another.

      - Examples: scoring a point, gaining relative position, an opponent losing relative position, impeding the progress of an opponent, rendering an opponent unable to block, stopping the game clock, interrupting a star pass, etc.

- Gaining advantages through an illegal action warrants a penalty.
- Established position

   - A skater’s physical location and status on the track.

      - Example: Up vs down, in vs. out of bounds, in vs. out of play, and a legal vs. illegal starting position

   - A skater need not stand still to hold an established position, although if they change their status or trajectory they require a moment to establish a new position.

- Fouling out

   - The removal of a skater for the remainder of a game after accumulating seven penalties.

- Blocking zone

   - The part of a skater’s body that initiates a block. Can be legal or illegal (2.4.2).

- Target zone

   - The part of a targeted skater’s body that receives a block. Can be legal or illegal (2.4.1).

- Immediately

   - At the first legal opportunity

- Warning

   - A verbal (and sometimes physical) indication an individual or team is in danger of receiving a penalty for an illegal action.
