Lesson #3: Verbal cues and hand signals
=======================================

*by Stephen Lorimor / Axis of Stevil*

Watch `lesson #3a (verbal cues) <https://www.youtube.com/watch?v=u0u6QaF_pDc&t=0s&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7&index=3>`_ and `lesson 3b (hand signals) <https://www.youtube.com/watch?v=NbzJ5ji5qOg&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7&index=4&t=0s>`_ on YouTube.

Last updated: January 21, 2018


The basics
----------

- All penalties are issued using the following format:
  
   - (whistle) + team color + skater number “spelled out” + penalty (cue and hand signal) + report to the box hand signal
     
- Verbal cues (especially penalties) fall into three tiers called stars (one star, two star, etc.). They roughly correspond to basic, intermediate, and advanced cues.
  
   - One-star cues sometimes lack specificity but are appropriate for the beginning official.
   - Two-star cues are standardized cues that provide additional information that replace one-star basic cues in certain situations. They are appropriate for experienced officials.
     
   - Three-star cues are non-standardized and should be used as used as necessary to provide additional context. Advanced officials are expected to use these.
     
   - Example:
     
      - One star: “Gold one zero, illegal procedure”
      - Two stars: “Gold one zero, star pass violation”
      - Three stars: “Gold one zero, star pass violation, report as the jammer”
	
- Call loudly, clearly, and repeat hand signals as necessary (especially on the outside)

Penalties
---------

4.1.1 - Impact to an illegal target zone
........................................

.. hlist::
   :columns: 3
	     
   * Back block
   * High block
   * Low block

     
4.1.2 - Impact with an illegal blocking zone
............................................

.. hlist::
   :columns: 3
	     
   * Head block
   * Forearm
   * Leg block

     
4.1.3 - Other illegal contact
.............................

Illegal Contact, or
     
.. hlist::
   :columns: 3
	     
   * Illegal assist
   * Early hit
   * Late hit
   * Out of play block

     
Direction, or
     
.. hlist::
   :columns: 3

   - Stop block

      
4.1.4 - Multiplayer blocks
..........................
  
- Multiplayer

     
4.2.1 - Illegal positioning
...........................

Illegal position, or
     
.. hlist::
   :columns: 3

   - Failure to reform
   - Failure to return
   - Failure to yield
   - Destruction
   - Skating out of bounds

      
4.2.2 - Gaining position
........................

Cut, or
     
.. hlist::
   :columns: 3
     
   - Illegal re-entry

      
4.2.3 - Interfering with the flow of the game
.............................................

- Interference, or
     
   - Delay of game

      
4.2.4 - Other illegal procedures
................................

Illegal Procedure, or
     
.. hlist::
   :columns: 3
     
   - Star pass violation
   - Pass interference
	
4.3 - Unsporting conduct
........................

Misconduct, or
     
.. hlist::
   :columns: 3
     
   - Insubordination

      
Obsolete penalty cues
---------------------

- These were removed or heavily modified in the current system.
- They may resurface as three-star cues

  ======================                =====================           ===================
  Out of bounds block			Out of bounds assist		Destroying the pack
  Clockwise block			Clockwise assist		Stopped assist
  Illegal return			Elbows				Too many skaters
  Blocking with the head		Penalty box violation		Uniform violation
  Illegal call-off			Team staff violation		Stalling
  Leaping contact			Charging			Unsporting conduct
  Reckless entry			Embellishment		        Blocking while down
  ======================                =====================           ===================


Additional communications
-------------------------

Skater identification required
..............................

.. hlist::
   :columns: 3

   * Show the star
   * False start
   * Report to the box
   * Stay on the track
   * Return to the track
   * Go to your bench
   * Lead Jammer
   * Not lead jammer

     
No skater identification required
.................................

.. hlist::
   :columns: 3

   * Star pass complete
   * Team timeout
   * Official timeout
   * Official review
   * Jam is on
   * Multiple penalties

* No pack, or

  - No pack, split

* Pack is here, or

  - Pack is front / back / all / etc.

* Out of play, or

  - Out of play in front / back

      
Hand signals
------------

- Referees must know all hand signals
  
   - Expressively -- the ability to perform them correctly for others
   - Receptively -- the ability to recognize hand signals by others
     
- This is an appropriate time for referee trainees to learn standardized hand signals as well as hand signals used by the local officiating community.

  
Homework
--------

- Read and study the WFTDA Officiating Cues and Signals document
  
   - https://static.wftda.com/officiating/wftda-officiating-cues-codes-and-signals.pdf
     
- Practice calling penalties and doing the hand signals

   - https://roller-derby.frama.io/-/random-penalty-generator/-/jobs/2092254/artifacts/public/index.html

- Have a friend quiz you until you can pass this test
  
   - Print version: https://tinyurl.com/ycb4fun4
   - YouTube version: https://tinyurl.com/y8l4nhqw
