Lesson #4: Game concepts
========================

*by Stephen Lorimor / Axis of Stevil*

`Watch this lesson on YouTube <https://www.youtube.com/watch?v=7bR1BbVC5Hg&t=1s&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7&index=7>`_

Last updated: January 9, 2019


Game types
----------

- Sanctioned games are regulation games that count towards rankings within a specific organization.
  
   - WFTDA sanctions adult women’s derby
   - MRDA sanctions adult men’s derby
   - JRDA sanctions junior (18 and younger) derby.
     
      - JRDA has females only, males only, and open divisions.
	
   - In a sanctioned game, teams may have a maximum of 15 skaters.
     
- Regulation games are games played using the WFTDA rules, officiating procedures, and officiating cues/codes/signals with no deviations.
  
   - American (but not all international) games must also adhere to WFTDA safety guidelines.
   - In regulation and non-regulation game there is no maximum number of skaters.
     
      - Participating teams may agree upon a maximum in advance of the game.
	
- Other (aka non-regulation) games are those played that deviate from the regulation game requirements.
  
   - Example: 30-minute games.


In bounds, out of bounds, down, and upright
-------------------------------------------

- A skater is out of bounds if they touch beyond the track boundary line with any part of their body.
  
   - Exception: touching beyond the track boundary line with one hand or arm
   - The track boundary line itself is considered in bounds
     
- A skater is straddling when they are touching both in and out of bounds at the same time.
- A skater is down when they have both hands on the floor, one or both knees, or are in a prone position.
  
   - One hand or arm touching the floor is not down.
     
- A skater is upright if they are not down
  
   - Falling is upright until they are down.


The pack
--------

- The pack is the largest group of upright, in bounds blockers from both teams skating or standing in proximity.
  
   - If the pack is destroyed, a strict set of rules is enabled until it is reformed
     
- A no pack situation is when a single pack does not exist
  
   - This can be two or more equal-sized packs
     
- The engagement zone is an area stretching from 20 feet in front of the pack to 20 feet behind it.
  
   - This is where the bulk of gameplay takes place
   - Blockers must remain inside the engagement zone
     
      - An upright, in bounds blocker in the engagement zone is in play
      - Out of play blockers must immediately return to the engagement zone
	
   - The engagement zone does not extend out of bounds.


Jammers and lead jammer
-----------------------

- A jammer not wearing a helmet cover is an inactive jammer
  
   - Inactive jammers cannot earn passes towards scoring or lead jammer status
   - Inactive lead jammers cannot call off the jam
     
      - Exception: if the lead jammer’s helmet cover was removed through natural gameplay or an opponent’s action
	
   - An inactive jammer may still leave the engagement zone
   - An inactive jammer may put the jammer helmet cover on their head to become active
     
- The lead jammer is the first jammer who…
  
   - Establishes a superior position to the foremost in play Blocker, having already
   - Earned a pass on all other Blockers (except those ahead of the engagement zone)
     
- A jammer loses the ability to become lead jammer if they…
  
   - Remove their helmet cover
   - Receive a penalty
   - Exit the front of the engagement zone without first earning lead jammer
     
- A pivot-turned-jammer (following a star pass) cannot become lead jammer


Blocking and assisting
----------------------

- A block is physical contact made to an opponent, and any movement or placement of one’s body to impede the opponent's speed or movement
- The initiator of a block is the person who causes the block to occur.
  
   - A block can have multiple initiators.
     
- A positional block is a block without contact wherein a skater positions themself so as to impede an opponent.
  
   - Positional blocks need not be intentional
     
- An assist is physical contact to a teammate that affects their movement
- Blocking and assisting must occur while upright within the engagement zone
  
   - Exception: Jammer vs. jammer blocking
   - Exception: Counter-blocking while out of play

      - Counter-blocking beyond what is required for safety is considered a separate block 
   
   - Exception: Passively assisting while straddling the track
   - Other minor exceptions
     
- A block can be initiated with any part of the body
  
   - Exception: The head
   - Exception: Hands, forearms, and elbows
   - Exception: Legs below mid-thigh
     
- It is legal to block any part of an opponent’s body (ie; target zones)
  
   - Exception: Neck and head
   - Exception: The back, including the butt and back of the legs
   - Exception: Legs below mid-thigh


Other concepts
--------------

- Game flow is the natural progression of the game, ideally without undue delays
- If a team has five or fewer skaters (after injuries, foul-outs, etc.), the head referee may declare a forfeit. This is rare, and is done after consulting with both team captains.
- The teams and head referee may mutually suspend play due to safety concerns, problems with the venue or track, etc.
- There is no such thing as an inactive pivot. A pivot not wearing their pivot cover loses all privileges of being pivot until they again wear the cover.
