Lesson #2: Whistles
===================

*by Stephen Lorimor / Axis of Stevil*

`Watch this lesson on YouTube <https://www.youtube.com/watch?v=8Lcl4pL-mJ4&t=1s&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7&index=2>`_

Last updated: May 9, 2018


Whistles
--------

- (standard) Fox 40 Classic

   - On lanyard
   - On finger ring
     
- Fox makes several other acceptable models

   - Classic Mini is a smaller version of the Classic. Same tone, but 6 dB quieter.

   - Eclipse is the same sound and volume as Classic, but a sleeker design.
     
- Fox 40 makes several other whistles (Pearl, Sonik, etc.)

   - Avoid non-standard whistles unless specifically instructed to use them.
   - Skaters are trained to respond to the specific tone of the Fox 40 Classic, and may be confused by non-standard whistles.
     
- Covid tip: Fox now makes a whistle-mask for officials. (If you ever try one out, write to me and tell me how well it works.)


Signals
-------

- One short blast → Jam start
- Two short, rapid blasts → Lead jammer
- One long blast → Penalty
- Four rapid, short blasts (one set) → Beginning of a timeout (team or official)
- Four rapid, short blasts (three sets) → Jam end
  
   - Continue sets until skaters cease playing.
     
- Rolling whistle → End of timeout or period


Don’t forget
------------

- When being called off, other referees should attempt to exactly match the timing of the tweets.
- Whistles are not used when issuing penalties between jams.
- Whistles are not used with warnings.
- A jam continues until the fourth tweet of the first set of jam call-off whistles.


Bonus tip
---------

- If you have pockets on your game uniform, carry an extra whistle or two in case an official’s whistle breaks during a game.
