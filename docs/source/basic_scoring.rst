Lesson #15: Basic scoring
=========================

*by Stephen Lorimor / Axis of Stevil*

`Watch this lesson on YouTube <https://www.youtube.com/watch?v=g_7uq34dV6Y&t=1s&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7&index=17>`_

Last updated: January 9, 2019


Basic concepts
--------------

#. Points are earned when jammers lap opposing blockers in a legal manner.
#. To avoid being scored upon, an opposing blocker must either remain in front of the jammer, or cause the jammer to gain superior position without earning a legal pass (see below).
#. Points for blockers that violate #2 can be earned another way.

 
Earning passes
--------------

Jammers primarily earn points by passing opposing blockers.

- All passes are measured at the hips.
- The jammer must be upright, in bounds, and the pass must be legal.

   - If the jammer passes a blocker in an invalid fashion, the jammer can re-pass the blocker during that scoring trip to earn the point.

- The blocker being passed need not be upright, in bounds, etc.
- The blocker being passed can be on their way to the penalty box, but not yet off the track.

   - Once they’re off the track, they’re considered to be a NOTT point (see next lesson).


Basic scoring rules
-------------------

- Only jammers wearing a jammer helmet cover (the “star”) with a visible star can score points
- The jammer’s initial trip through the pack each jam does not earn points

   - Exception: Scoring during overtime jams begins with the initial trip through the pack.

- Subsequent trips through the pack are scoring trips

   - A scoring trip ends and a new one begins when the jammer exits the front of the engagement zone.

      - During a no pack situation, a scoring trip ends upon passing the foremost blocker.

   - Re-entering the engagement zone from the front returns the jammer to the previous trip.

      - The jammer cannot score additional points on blockers until they return to the current trip by exiting the engagement zone to the front.
      - There is no lap count below the previous trip. (ie; A jammer cannot be two trips down regardless of how many times the pack laps them.)

- A jammer sent to the penalty box returns to the track on that same trip.

   - Exception: New jams always begins with a fresh initial trip.

- A maximum of one point per opposing blocker per scoring trip can be earned.
- Points are earned until the fourth tweet of the first set of jam call-off whistles.


Airborne passes
---------------

A jammer can pass an opposing skater while airborne (ie; jumping).

- The jammer must launch from in bounds.
- The jammer must be in bounds and upright at the instant of landing.
- The jammer must land before the fourth tweet of the jam call-off signal.
