Lesson #26: Assists
===================

*by Stephen Lorimor / Axis of Stevil*

`Watch this lesson on YouTube <https://www.youtube.com/watch?v=PzpA1mds1lI&index=31&list=PLRg6cN0RqKNomUbKTHw1ITXLT7pPQ9GX7&t=0s>`_

Last updated: June 30, 2018


Basic concepts
--------------

- (glossary definition) Physically affecting a teammate. Common examples include a push or a whip.

   - An assist off an opponent is considered to be a block

- All assists have an initiator and a recipient

   - These are sometimes the same person

- An active participant of an assist is a skater actively causing it to occur

   - Example: Offering or accepting an arm whip

- A passive participant of an assist is a skater passively allowing it to occur

   - Example: The skater whose hips are grabbed during a hip whip
   - Not all assists have a passive participant


Legal and illegal assists
-------------------------

- Verbal cue: “Illegal assist”.  This is a type of “Illegal contact” penalty.
- The initiator of an assist is judged for legality in the same way as the initiator of a block.

   - The initiator of an assist must be…

      - Upright
      - Fully in bounds
      - Inside the engagement zone
      - Moving counterclockwise or perpendicular to the track

- The second participant in the assist is judged in the same manner as the target of a block.

   - The second participant in the assist must be

      - Upright 

- Exception: helping a downed skater upright in the engagement zone should be considered an act of good sportsmanship

      - Straddling the track or fully in bounds
      - Inside the engagement zone

- Impact to call a penalty

   - The recipient of the assist gains position on a teammate or opponent.


Types of assists
----------------

   - Arm and/or hand whip
   - Hip and/or jersey whips
   - Peg assist

      - Usually done at jam start
      - A (usually) stopped skater near a track line reaches around the side of the track and grabs their team’s jammer behind an opposing wall between them. The jammer jumps toward the out of bounds area and the stopped skater pulls them in a semi-circle until the jammer lands in bounds.
      - In theory can be done legally, but is extremely difficult to do.

   - “Fauxpex” jump

      - An apex jump in which a skater propels themself forward using their hand on the shoulder of a teammate.
